<div class="m-md-5 ml-3 mr-3 mt-3" xmlns:livewire="">
    <div class="card-header border-0">
        <h2 class="mb-0">Porosite</h2>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="id">Numri Identifikues</th>
                <th scope="col" class="sort" data-sort="consumer">Klienti</th>
                <th scope="col" class="sort" data-sort="total">Totali</th>
                <th scope="col" class="sort" data-sort="address">Adresa</th>
                <th scope="col" class="sort" data-sort="status">Statusi</th>
                @if(auth()->user()->isAdmin())
                    <th scope="col" class="sort" data-sort="status">Dyqani</th>
                @endif
                <th scope="col" class="sort" data-sort="action">Veprim</th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($orders as $order)
                <livewire:order-list-row :order="$order"/>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
