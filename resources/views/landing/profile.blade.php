<div class="wrapper" xmlns:wire="http://www.w3.org/1999/xhtml" xmlns:livewire="">
    <div class="section section-components ml-md-5 mr-md-5 ml-3 mr-3 mt-3">
        <div class="mb-5">
            <!-- Inputs -->
            <h3 class="h4 text-success font-weight-bold mb-4">Profili juaj</h3>
            <livewire:edit-profile/>
            <h3 class="h4 text-success font-weight-bold mb-4 mt-5">Ndrysho fjalekalimin</h3>
            <livewire:change-password/>
            @if(auth()->user()->isConsumer())
                <h3 class="h4 text-success font-weight-bold mb-4 mt-5">Historiku i porosive</h3>
                <div class="container p-0 rounded bg-white mt-2">
                    <ul class="list-group">
                        @foreach($orders as $i => $order)
                            <a href="{{route('order-completion', ['id' => $order['id']])}}">
                                <li class="list-group-item" style="cursor: pointer;">
                                    <h5>Porosia me numer identifikimi #{{$order['id']}}</h5>
                                    <div>Dyqani: {{$order['store']['name']}}</div>
                                    <span class="badge badge-dot mr-4 mb-2 mt-2">
                                    <span
                                        class="status m-0 p-0 badge badge-pill @if($order['status'] === \App\Models\Order::$PENDING_STATUS) badge-danger @endif
                                        @if($order['status'] === \App\Models\Order::$ACCEPTED_STATUS) badge-primary @endif
                                        @if($order['status'] === \App\Models\Order::$REJECTED_STATUS) badge-dark @endif
                                        @if($order['status'] === \App\Models\Order::$COMPLETED_STATUS) badge-success @endif">
                                        <p class="m-0 px-3 py-1 font-weight-bold">{{$order['status']}}</p>
                                    </span>
                                  </span>
                                    <h5 class="text-right">Totali: {{$order['total_price']}} Lek</h5>
                                </li>
                            </a>
                        @endforeach
                        @if(empty($orders))
                            Ju nuk keni bere asnje porosi.
                        @endif
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
