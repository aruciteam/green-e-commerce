@section('content')
    <div class="row mx-2 mx-sm-4 mt-5" xmlns:livewire="">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <h4 class="modal-title mb-5" id="modal-title-default">Shporta juaj</h4>
            @include('components.shopping-cart-content', ['total' => $total, 'products' => $products])
            <livewire:cart-form />
        </div>
        <div class="col-lg-2"></div>
    </div>
@endsection
