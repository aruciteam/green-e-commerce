@extends('layouts.landing-layout')

@section('content')
    <div class="row mt-md-5 mt-3 mr-3 ml-3">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6 text-center">
            <h1 class="mb-5">Kushtet e privatesise</h1>
            <div class="row text-center">
                <p>
                    Në Ferma Online, e aksesueshme nga www.fermaonline.al, një nga prioritetet tona kryesore është
                    privatësia e vizitorëve tanë. Ky dokument i Politikës së Privatësisë përmban lloje të informacionit
                    që mblidhet dhe regjistrohet nga Ferma Online dhe si e përdorim atë.
                </p>
                <p>
                    Nëse keni pyetje shtesë ose kërkoni më shumë informacion në lidhje me Politikën tonë të Privatësisë,
                    mos
                    hezitoni të na kontaktoni.
                </p>
                <p>
                    Kjo Politikë e Privatësisë zbatohet vetëm për aktivitetet tona në internet dhe është e vlefshme për
                    vizitorët në faqen tonë të internetit në lidhje me informacionin që ata kanë ndarë dhe/ose mbledhin
                    në
                    Ferma Online. Kjo politikë nuk zbatohet për asnjë informacion të mbledhur jashtë linje ose nëpërmjet
                    kanaleve të tjera përveç kësaj faqe interneti. Politika jonë e privatësisë u krijua me ndihmën e
                    Gjeneruesit Falas të Politikave të Privatësisë.
                </p>
                <p>
                    Pëlqimi
                    Duke përdorur faqen tonë të internetit, ju pranoni politikën tonë të privatësisë dhe pranoni kushtet
                    e
                    saj.
                </p>
                <p>
                    Informacioni që mbledhim
                    Informacioni personal që ju kërkohet të jepni dhe arsyet pse ju kërkohet t'i jepni ato, do t'ju
                    bëhen të
                    qarta në momentin kur ne ju kërkojmë të jepni informacionin tuaj personal.
                </p>
                <p>
                    Nëse na kontaktoni drejtpërdrejt, ne mund të marrim informacion shtesë rreth jush, si emrin, adresën
                    e
                    emailit, numrin e telefonit, përmbajtjen e mesazhit dhe/ose bashkëngjitjet që mund të na dërgoni,
                    dhe
                    çdo informacion tjetër që mund të zgjidhni të jepni.
                </p>
                <p>
                    Kur regjistroheni për një llogari, ne mund të kërkojmë informacionin tuaj të kontaktit, duke
                    përfshirë
                    artikuj të tillë si emri, emri i kompanisë, adresa, adresa e emailit dhe numri i telefonit.
                </p>
                <p>
                    Si e përdorim informacionin tuaj
                    Ne përdorim informacionin që mbledhim në mënyra të ndryshme, duke përfshirë:
                </p>
                <p>
                    Siguroni, operoni dhe mirëmbani faqen tonë të internetit
                    Përmirësoni, personalizoni dhe zgjeroni faqen tonë të internetit
                    Kuptoni dhe analizoni se si e përdorni faqen tonë të internetit
                    Zhvilloni produkte, shërbime, veçori dhe funksionalitete të reja
                    Komunikoni me ju, ose drejtpërdrejt ose përmes një prej partnerëve tanë, duke përfshirë shërbimin
                    ndaj
                    klientit, për t'ju ofruar përditësime dhe informacione të tjera në lidhje me faqen e internetit, dhe
                    për
                    qëllime marketingu dhe promovimi
                    Ju dërgoj email
                    Gjeni dhe parandaloni mashtrimin
                    Skedarët e regjistrit
                    Ferma Online ndjek një procedurë standarde të përdorimit të skedarëve log. Këta skedarë regjistrojnë
                    vizitorët kur ata vizitojnë faqet e internetit. Të gjitha kompanitë pritëse e bëjnë këtë dhe një
                    pjesë e
                    analitikës së shërbimeve të pritjes. Informacioni i mbledhur nga skedarët e regjistrave përfshin
                    adresat
                    e protokollit të internetit (IP), llojin e shfletuesit, Ofruesin e Shërbimit të Internetit (ISP),
                    vulën
                    e datës dhe orës, faqet referuese/dalëse dhe mundësisht numrin e klikimeve. Këto nuk janë të lidhura
                    me
                    asnjë informacion që është personalisht i identifikueshëm. Qëllimi i informacionit është analizimi i
                    tendencave, administrimi i faqes, gjurmimi i lëvizjes së përdoruesve në faqen e internetit dhe
                    mbledhja
                    e informacionit demografik.
                </p>
                <p>
                    Politikat e Privatësisë së Partnerëve të Reklamimit
                    Ju mund të konsultoni këtë listë për të gjetur Politikën e Privatësisë për secilin prej partnerëve
                    reklamues të Ferma Online.
                </p>
                <p>
                    Serverët e reklamave të palëve të treta ose rrjetet e reklamave përdorin teknologji si cookies,
                    JavaScript ose Web Beacons që përdoren në reklamat e tyre përkatëse dhe lidhjet që shfaqen në Ferma
                    Online, të cilat dërgohen drejtpërdrejt në shfletuesin e përdoruesve. Ata marrin automatikisht
                    adresën
                    tuaj IP kur kjo ndodh. Këto teknologji përdoren për të matur efektivitetin e fushatave të tyre
                    reklamuese dhe/ose për të personalizuar përmbajtjen reklamuese që shihni në faqet e internetit që
                    vizitoni.
                </p>
                <p>
                    Vini re se Ferma Online nuk ka akses ose kontroll mbi këto skedarë kuki që përdoren nga reklamues të
                    palëve të treta.
                </p>
                <p>
                    Politikat e privatësisë së palëve të treta
                    Politika e privatësisë e Ferma Online nuk zbatohet për reklamuesit ose faqet e internetit të tjera.
                    Kështu, ne po ju këshillojmë që të konsultoheni me Politikat përkatëse të Privatësisë të këtyre
                    serverëve të reklamave të palëve të treta për informacion më të detajuar. Mund të përfshijë
                    praktikat
                    dhe udhëzimet e tyre se si të tërhiqeni nga disa opsione.
                </p>
                <p>
                    Ju mund të zgjidhni të çaktivizoni cookies përmes opsioneve individuale të shfletuesit tuaj. Për të
                    ditur më shumë informacion të detajuar rreth menaxhimit të cookie-ve me shfletues të veçantë të
                    internetit, ai mund të gjendet në faqet e internetit përkatëse të shfletuesve.
                </p>
                <p>
                    Të drejtat e privatësisë CCPA (Mos i shit të dhënat e mia personale)
                    Sipas CCPA, ndër të drejtat e tjera, konsumatorët e Kalifornisë kanë të drejtë të:
                </p>
                <p>
                    Kërkoni që një biznes që mbledh të dhënat personale të një konsumatori të zbulojë kategoritë dhe
                    pjesët
                    specifike të të dhënave personale që një biznes ka mbledhur për konsumatorët.
                </p>
                <p>
                    Kërkoni që një biznesi të fshijë çdo të dhënë personale për konsumatorin që një biznes ka mbledhur.
                </p>
                <p>
                    Kërkoni që një biznes që shet të dhënat personale të një konsumatori, të mos shesë të dhënat
                    personale
                    të konsumatorit.
                </p>
                <p>
                    Nëse bëni një kërkesë, ne kemi një muaj kohë për t'ju përgjigjur. Nëse dëshironi të ushtroni ndonjë
                    nga
                    këto të drejta, ju lutemi na kontaktoni.
                </p>
                <p>
                    Të drejtat e mbrojtjes së të dhënave GDPR
                    Ne dëshirojmë të sigurohemi që ju jeni plotësisht të vetëdijshëm për të gjitha të drejtat tuaja për
                    mbrojtjen e të dhënave. Çdo përdorues ka të drejtë për sa vijon:
                </p>
                <p>
                    E drejta për akses – Ju keni të drejtë të kërkoni kopje të të dhënave tuaja personale. Ne mund t'ju
                    kërkojmë një tarifë të vogël për këtë shërbim.
                </p>
                <p>
                    E drejta për korrigjim – Ju keni të drejtën të kërkoni që ne të korrigjojmë çdo informacion që
                    mendoni
                    se është i pasaktë. Ju gjithashtu keni të drejtë të kërkoni që ne të plotësojmë informacionin që
                    mendoni
                    se është i paplotë.
                </p>
                <p>
                    E drejta e fshirjes – Ju keni të drejtën të kërkoni që ne të fshijmë të dhënat tuaja personale, në
                    kushte të caktuara.
                </p>
                <p>
                    E drejta për të kufizuar përpunimin – Ju keni të drejtë të kërkoni që ne të kufizojmë përpunimin e
                    të
                    dhënave tuaja personale, në kushte të caktuara.
                </p>
                <p>
                    E drejta për të kundërshtuar përpunimin – Ju keni të drejtë të kundërshtoni përpunimin tonë të të
                    dhënave tuaja personale, në kushte të caktuara.
                </p>
                <p>
                    E drejta e transportueshmërisë së të dhënave – Ju keni të drejtë të kërkoni që ne t'i transferojmë
                    të
                    dhënat që kemi mbledhur në një organizatë tjetër, ose drejtpërdrejt te ju, në kushte të caktuara.
                </p>
                <p>
                    Nëse bëni një kërkesë, ne kemi një muaj kohë për t'ju përgjigjur. Nëse dëshironi të ushtroni ndonjë
                    nga
                    këto të drejta, ju lutemi na kontaktoni.
                </p>
                <p>
                    Informacion për Fëmijët
                    Një pjesë tjetër e prioritetit tonë është shtimi i mbrojtjes për fëmijët gjatë përdorimit të
                    internetit.
                    Ne inkurajojmë prindërit dhe kujdestarët të vëzhgojnë, të marrin pjesë dhe/ose të monitorojnë dhe
                    drejtojnë aktivitetin e tyre në internet.
                </p>
                <p>
                    Ferma Online nuk mbledh me vetëdije asnjë informacion personal të identifikueshëm nga fëmijët nën
                    moshën
                    13 vjeç. Nëse mendoni se fëmija juaj ka dhënë këtë lloj informacioni në faqen tonë të internetit, ju
                    inkurajojmë fuqimisht të na kontaktoni menjëherë dhe ne do të bëjmë çdo përpjekje për të hiqni një
                    informacion të tillë nga të dhënat tona.
                </p>
            </div>
        </div>
        <div class="col-lg-3">
        </div>
    </div>
@endsection
