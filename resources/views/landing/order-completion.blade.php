@section('content')
    <div class="row mx-2 mx-sm-4 mt-5">
        @if($isFromCart)
            <div class="col-lg-5">
                <div class="info">
                    <div class="icon icon-xl icon-shape icon-shape-success shadow rounded-circle">
                        <i class="fas fa-check fa-9x"></i>
                    </div>
                    <h5 class="info-title text-uppercase text-success">Porosia juaj u dergua me sukses</h5>
                    <p class="description opacity-8">Dyqani eshte lajmeruar per porosine tuaj. Ju do te kontaktoheni
                        ne momentin qe do te kryhet dergesa.</p>
                    <p class="description opacity-8">Nese deshironi te kontaktoni
                        dyqanin per cfaredolloj pyetje mund te kontaktoni ne
                        numrin: {{$order['store']['phone_number']}}</p>
                    <p class="description opacity-8">Nese deshironi te kontaktoni stafin e ferma jeshile per cfaredolloj
                        pyetje
                        mmund te kontaktoni ne numrin: +355699312253</p>
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        @else
            <div class="col-lg-3">
            </div>
        @endif

        <div class="col-lg-6 mt-lg-0 mt-4">
            <h4 class="modal-title" id="modal-title-default">Porosia juaj</h4>
            <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <span class="status badge badge-pill @if($order['status'] === \App\Models\Order::$PENDING_STATUS) badge-danger @endif
                        @if($order['status'] === \App\Models\Order::$ACCEPTED_STATUS) badge-primary @endif
                        @if($order['status'] === \App\Models\Order::$REJECTED_STATUS) badge-dark @endif
                        @if($order['status'] === \App\Models\Order::$COMPLETED_STATUS) badge-success @endif">
                            <p class="m-0 px-2 font-weight-bold">{{$order['status']}}</p>
                        </span>
                      </span>
            @include('components.order-info', ['order' => $order])
        </div>

        @if(!$isFromCart)
            <div class="col-lg-3">
            </div>
        @endif

    </div>
@endsection
