@extends('layouts.landing-layout')

@section('content')
<div class="row mt-md-5 mt-3 mr-3 ml-3">
    <div class="col-lg-3">
    </div>
    <div class="col-lg-6 text-center">
        <h1 class="mb-5">Partneret</h1>
        <div class="row">
            <div class="col-md-6 col-12">
                <a href="https://www.shav.al/sq/" target="_blank">
                    <img src="{{ asset('assets') }}/img/shav_logo.png" class="" style="height: 80px;">
                </a>
            </div>
            <div class="col-md-6 col-12 m-md-0 mt-4">
                <a href="https://www.bashkiadivjake.gov.al/home-page/" target="_blank">
                    <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="mb-1" style="height: 80px;">
                    <p>Bashkia Divjake</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
    </div>

</div>
@endsection
