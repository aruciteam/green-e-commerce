@section('content')
    <div class="wrapper" xmlns:livewire="">
        <div class="section section-hero" style="background-color: #9fd46a">
            <div class="page-header">
                <div class="container  d-flex align-items-center ">
                    <div class="col px-0">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-lg-8 text-center">
                                <h1 class="text-white font-weight-bold mb-4">Ferma online me produkte te
                                    fresketa</h1>
                                <livewire:search-product-categories/>
                                <img src="{{ asset('argon-design-system') }}/img/brand/blue.png" style="width: 200px;"
                                     class="img-fluid mt-4 mr-4 ml-4" alt="">
                                <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="mt-4 mr-4 ml-4"
                                     style="width: 135px; height: 150px">
                                <p class="lead text-white font-weight-bold">100% Produkte Bio</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                     xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <div class="section features-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mx-auto text-center">
                        <span class="badge badge-primary badge-pill mb-3">Informacion</span>
                        <h3 class="display-3">Si funksionon?</h3>
                        <p class="lead">Kjo eshte platforma ku ju mund te porosisni ushqimet tuaja te preferuara nga
                            fermeret tane.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="info text-center">
                            <div class="icon icon-lg icon-shape icon-shape-primary shadow rounded-circle">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <h6 class="info-title text-uppercase text-primary">Shtoni produktet ne shporte</h6>
                            <p class="description opacity-8">Shqyrtoni varietetin e gjere te produkteve bio te mbledhur
                                dhe pergatitur me dashuri nga shitesit tane partnere dhe shtoni produktet qe deshironi
                                te bleni ne shporten tuaj.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info text-center">
                            <div class="icon icon-lg icon-shape icon-shape-success shadow rounded-circle">
                                <i class="far fa-paper-plane"></i></div>
                            <h6 class="info-title text-uppercase text-success">Porosisni me te dhenat tuaja</h6>
                            <p class="description opacity-8">Krijoni profilin tuaj dhe vendosni te dhenat me ane te te
                                cilave deshironi te kontaktoheni dhe me pas dergoni porosine.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info text-center">
                            <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle">
                                <i class="fas fa-home"></i>
                            </div>
                            <h6 class="info-title text-uppercase text-warning">Ushqimi bio vjen ne shtepine tuaj</h6>
                            <p class="description opacity-8">Shitesi do te informohet per porosine tuaj dhe do sjelle
                                produktin ne adresen ku ju e kerkuat. E gjitha kjo, vetem me 3 hapa te shpejte!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ml-md-5 mr-md-5 ml-3 mr-3">
            <div class="section section-components pb-0" id="section-components">
                @include('components.product-categories-list', ['categories' => $categories])
            </div>
            <div class="section section-components pb-0" id="section-components">
                @include('components.stores-list', ['stores' => $stores])
            </div>
            <div class="section section-components pb-0" id="section-components">
                @include('components.products-list', ['products' => $products, 'canBeAddedToCart' => false])
            </div>
        </div>
    </div>
@endsection

