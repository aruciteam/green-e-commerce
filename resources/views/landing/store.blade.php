@extends('layouts.landing-layout')

@section('content')
    <div>
        <img class="store-img mb-2"
             src="{{$store['image_url']}}">
        <div class="ml-md-5 mr-md-5 ml-3 mr-3">
            <p class="store-name">
                {{$store['name']}}
            </p>
            <p class="store-description">
                {{$store['description']}}
            </p>
            <p class="store-description">
                Transporti: {{$store['delivery_price']}} Lek
            </p>
            <p class="store-description">
                Adresa: {{$store['address']}}
            </p>
            <div class="mt-5">
                @include('components.products-list', ['products' => $store['products'], 'canBeAddedToCart' => true])
            </div>
        </div>

    </div>

@endsection
