@extends('layouts.landing-layout')

@section('content')
    <div>
        <div class="product-category-title-wrapper mb-2 text-center"  style="background-color: {{$category['color']}}">
            <div class="m-auto text-center">
                <p class="store-name text-white">
                    {{$category['name']}}
                </p>
            </div>

        </div>
        <div class="ml-md-5 mr-md-5 ml-3 mr-3">

            <div class="mt-5">
                @include('components.products-list', ['products' => $category['products'], 'canBeAddedToCart' => false])
            </div>
        </div>

    </div>

@endsection
