@extends('layouts.landing-layout')

@section('content')
    <div class="row mt-md-5 mt-3 mr-3 ml-3">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6 text-center">
            <h1 class="mb-5">Kontakti</h1>
            <div class="row text-center">
                <p class="mb-5">
                    Bashkia e re Divjakë kufizohet në veri me bashkinë Rrogozhinë,
                    në lindje me bashkinë Lushnjë, në jug me bashkinë Fier dhe në
                    perëndim me Detin Adriatik. Kryeqendra e bashkisë së re do të
                    jetë qyteti i Divjakës.
                </p>
                <div class="m-md-0 mt-4" style="width: 100%">
                    <div class="mb-5">
                        <i class="fas fa-map-marker-alt fa-lg mb-3"></i>
                        <div class="mb-4">Prane Sheshit “Santa Barbara”</div>
                    </div>

                    <div class="mb-5">
                        <i class="fas fa-phone fa-lg mb-3"></i>
                        <div class="mb-4">+355 69 93 12 253</div>
                    </div>

                    <div class="mb-5">
                        <i class="fas fa-envelope fa-lg mb-3"></i>
                        <div class="mb-4">fermaonline@bashkiadivjake.gov.al</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
        </div>

    </div>
@endsection
