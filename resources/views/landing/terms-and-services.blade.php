@extends('layouts.landing-layout')

@section('content')
    <div class="row mt-md-5 mt-3 mr-3 ml-3">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6 text-center">
            <h1 class="mb-5">Termat e perdorimit</h1>
            <div class="row text-center">
                <p>
                    Këto terma dhe kushte përshkruajnë rregullat dhe rregulloret për përdorimin e faqes së internetit të
                    Ferma Online, e vendosur në www.fermaonline.al.
                </p>
                <p>
                    Duke hyrë në këtë faqe interneti, ne supozojmë se ju i pranoni këto terma dhe kushte. Mos vazhdoni
                    të përdorni Ferma Online nëse nuk pranoni të zbatoni të gjitha termat dhe kushtet e përcaktuara në
                    këtë faqe.
                </p>
                <p>
                    Terminologjia e mëposhtme zbatohet për këto Kushte dhe Kushte, Deklaratë Privatësie dhe Njoftim për
                    Mohim përgjegjësie dhe të gjitha Marrëveshjet: "Klienti", "Ju" dhe "Juaj" ju referohen juve,
                    personit që hyni në këtë faqe interneti dhe është në përputhje me termat dhe kushtet e Kompanisë.
                    "Kompania", "Vetja jonë", "Ne", "Ynë" dhe "Ne", i referohet kompanisë sonë. "Pala", "Palët", ose
                    "Ne", i referohet si Klientit ashtu edhe neve. Të gjitha kushtet i referohen ofertës, pranimit dhe
                    shqyrtimit të pagesës të nevojshme për të ndërmarrë procesin e ndihmës sonë për Klientin në mënyrën
                    më të përshtatshme për qëllimin e shprehur të plotësimit të nevojave të Klientit në lidhje me
                    ofrimin e shërbimeve të deklaruara të Kompanisë, në përputhje me dhe i nënshtrohet ligjit në fuqi të
                    Holandës. Çdo përdorim i terminologjisë së mësipërme ose fjalëve të tjera në njëjës, shumës,
                    shkronja të mëdha dhe/ose ai/ajo ose ato, merren si të këmbyeshëm dhe për rrjedhojë si referim të
                    njëjtë.
                </p>
                <p>
                    Biskota
                    Ne përdorim përdorimin e cookies. Duke hyrë në Ferma Online, ju pranuat të përdorni cookie në
                    marrëveshje me Politikën e Privatësisë së Ferma Online.
                </p>
                <p>
                    Shumica e faqeve interaktive të internetit përdorin cookie për të na lejuar të marrim të dhënat e
                    përdoruesit për çdo vizitë. Cookies përdoren nga faqja jonë e internetit për të mundësuar
                    funksionalitetin e zonave të caktuara për ta bërë më të lehtë për njerëzit që vizitojnë faqen tonë
                    të internetit. Disa nga filialet/partnerët tanë reklamues mund të përdorin gjithashtu cookie.
                </p>
                <p>
                    Liçensë
                    Nëse nuk përcaktohet ndryshe, Ferma Online dhe/ose licencuesit e saj zotërojnë të drejtat e
                    pronësisë intelektuale për të gjithë materialin në Ferma Online. Të gjitha të drejtat e pronësisë
                    intelektuale janë të rezervuara. Ju mund ta përdorni këtë nga Ferma Online për përdorimin tuaj
                    personal, duke iu nënshtruar kufizimeve të përcaktuara në këto terma dhe kushte.
                </p>
                <p>
                    Ju nuk duhet:
                    Ripublikoni materialin nga Ferma Online
                    Shisni, merrni me qira ose nën-licenconi materiale nga Ferma Online
                    Riprodhoni, kopjoni ose kopjoni materiale nga Ferma Online
                    Rishpërndani përmbajtjen nga Ferma Online
                    Kjo Marrëveshje do të fillojë në datën e kësaj. Termat dhe Kushtet tona u krijuan me ndihmën e
                    Modelit të Termave dhe Kushteve.
                </p>
                <p>
                    Pjesë të kësaj faqeje interneti ofrojnë një mundësi për përdoruesit që të postojnë dhe shkëmbejnë
                    mendime dhe informacione në fusha të caktuara të faqes së internetit. Ferma Online nuk filtron,
                    modifikon, publikon ose rishikon komentet përpara pranisë së tyre në faqen e internetit. Komentet
                    nuk pasqyrojnë pikëpamjet dhe opinionet e Ferma Online, agjentëve dhe/ose bashkëpunëtorëve të saj.
                    Komentet pasqyrojnë pikëpamjet dhe opinionet e personit që poston pikëpamjet dhe opinionet e tyre.
                    Në masën e lejuar nga ligjet në fuqi, Ferma Online nuk do të jetë përgjegjëse për komentet ose për
                    ndonjë përgjegjësi, dëmtim ose shpenzim të shkaktuar dhe/ose të pësuar si rezultat i çdo përdorimi
                    dhe/ose postimi dhe/ose shfaqja e komenteve në këtë faqe interneti.
                </p>
                <p>
                    Ferma Online rezervon të drejtën të monitorojë të gjitha komentet dhe të heqë çdo koment që mund të
                    konsiderohet i papërshtatshëm, fyes ose shkakton shkelje të këtyre Kushteve dhe Kushteve.
                </p>
                <p>
                    Ju garantoni dhe përfaqësoni se:
                    Ju keni të drejtë të postoni komentet në faqen tonë të internetit dhe keni të gjitha licencat dhe
                    pëlqimet e nevojshme për ta bërë këtë;
                    Komentet nuk cenojnë asnjë të drejtë të pronësisë intelektuale, duke përfshirë pa kufizim të drejtën
                    e autorit, patentën ose markën tregtare të ndonjë pale të tretë;
                    Komentet nuk përmbajnë asnjë material shpifës, shpifës, fyes, të pahijshëm ose ndryshe të paligjshëm
                    që është një cenim i privatësisë
                    Komentet nuk do të përdoren për të kërkuar ose promovuar biznes ose me porosi ose për të paraqitur
                    aktivitete tregtare ose aktivitete të paligjshme.
                    Në këtë mënyrë ju i jepni Ferma Online një licencë joekskluzive për të përdorur, riprodhuar,
                    modifikuar dhe autorizuar të tjerët për të përdorur, riprodhuar dhe modifikuar ndonjë nga komentet
                    tuaja në çdo formë, format ose media.
                </p>
                <p>
                    Hiperlidhja me përmbajtjen tonë
                    Organizatat e mëposhtme mund të lidhen me faqen tonë të internetit pa miratim paraprak me shkrim:
                </p>
                <p>
                    Agjencitë qeveritare;
                    Motorë kërkimi;
                    Organizatat e lajmeve;
                    Shpërndarësit e direktoriumeve në internet mund të lidhen me faqen tonë të internetit në të njëjtën
                    mënyrë siç ata lidhen me faqet e internetit të bizneseve të tjera të listuara; dhe
                    Biznese të akredituara në të gjithë sistemin, përveç kërkimit të organizatave jofitimprurëse,
                    qendrave tregtare bamirësie dhe grupeve për mbledhjen e fondeve bamirësie, të cilat mund të mos
                    lidhen me faqen tonë të internetit.
                    Këto organizata mund të lidhen me faqen tonë kryesore, me botime ose me informacione të tjera të
                    Uebsajtit për sa kohë që lidhja: (a) nuk është në asnjë mënyrë mashtruese; (b) nuk nënkupton në
                    mënyrë të rreme sponsorizimin, miratimin ose miratimin e palës lidhëse dhe produkteve dhe/ose
                    shërbimeve të saj; dhe (c) përshtatet brenda kontekstit të sajtit të palës lidhëse.
                </p>
                <p>
                    Ne mund të shqyrtojmë dhe të miratojmë kërkesa të tjera për lidhje nga llojet e mëposhtme të
                    organizatave:
                    burime informacioni të njohura për konsumatorin dhe/ose biznesin;
                    faqet e komunitetit dot.com;
                    shoqata ose grupe të tjera që përfaqësojnë bamirësi;
                    shpërndarësit e direktoriumeve online;
                    portalet e internetit;
                    kontabilitet, kompani ligjore dhe konsulente; dhe
                    institucionet arsimore dhe shoqatat tregtare.
                    Ne do të miratojmë kërkesat për lidhje nga këto organizata nëse vendosim që: (a) lidhja nuk do të na
                    bënte të dukemi të pafavorshëm për veten ose për bizneset tona të akredituara; (b) organizata nuk ka
                    të dhëna negative me ne; (c) përfitimi për ne nga dukshmëria e hiperlidhjes kompenson mungesën e
                    Ferma Online; dhe (d) lidhja është në kontekstin e informacionit të burimeve të përgjithshme.
                </p>
                <p>
                    Këto organizata mund të lidhen me faqen tonë kryesore për sa kohë që lidhja: (a) nuk është në asnjë
                    mënyrë mashtruese; (b) nuk nënkupton në mënyrë të rreme sponsorizimin, miratimin ose miratimin e
                    palës lidhëse dhe produkteve ose shërbimeve të saj; dhe (c) përshtatet brenda kontekstit të sajtit
                    të palës lidhëse.
                </p>
                <p>
                    Nëse jeni një nga organizatat e listuara në paragrafin 2 më sipër dhe jeni të interesuar të lidheni
                    me faqen tonë të internetit, duhet të na informoni duke dërguar një e-mail në Ferma Online. Ju
                    lutemi përfshini emrin tuaj, emrin e organizatës suaj, informacionin e kontaktit si dhe URL-në e
                    faqes suaj, një listë të çdo URL-je nga e cila keni ndërmend të lidheni me faqen tonë të internetit
                    dhe një listë të URL-ve në faqen tonë në të cilat dëshironi të lidhje. Prisni 2-3 javë për një
                    përgjigje.
                </p>
                <p>
                    Organizatat e miratuara mund të lidhen me faqen tonë të internetit si më poshtë:
                    Duke përdorur emrin e korporatës tonë; ose
                    Me përdorimin e gjetësit uniform të burimeve që lidhen me; ose
                    Përdorimi i çdo përshkrimi tjetër të faqes sonë të internetit që lidhet me këtë ka kuptim brenda
                    kontekstit dhe formatit të përmbajtjes në sajtin e palës lidhëse.
                    Nuk do të lejohet përdorimi i logos së Ferma Online ose veprave të tjera artistike për lidhjen në
                    mungesë të një marrëveshjeje licence për markën tregtare.
                </p>
                <p>
                    iFrames
                    Pa miratim paraprak dhe leje me shkrim, ju nuk mund të krijoni korniza rreth faqeve tona të
                    internetit që ndryshojnë në asnjë mënyrë paraqitjen vizuale ose pamjen e faqes sonë të internetit.
                </p>
                <p>
                    Përgjegjësia për përmbajtjen
                    Ne nuk do të mbajmë përgjegjësi për çdo përmbajtje që shfaqet në faqen tuaj të internetit. Ju
                    pranoni të na mbroni dhe të na mbroni kundër të gjitha pretendimeve që ngrihen në faqen tuaj të
                    internetit. Asnjë lidhje (lidhje) nuk duhet të shfaqet në asnjë faqe interneti që mund të
                    interpretohet si shpifëse, e turpshme ose kriminale, ose që cenon, përndryshe shkel ose mbron
                    shkeljen ose shkeljen tjetër të ndonjë të drejte të palës së tretë.
                </p>
                <p>
                    Privatësia juaj
                    Ju lutemi lexoni Politikën e Privatësisë
                    Rezervimi i të Drejtave
                    Ne rezervojmë të drejtën të kërkojmë që të hiqni të gjitha lidhjet ose ndonjë lidhje të veçantë në
                    faqen tonë të internetit. Ju miratoni të hiqni menjëherë të gjitha lidhjet në faqen tonë të
                    internetit sipas kërkesës. Ne gjithashtu rezervojmë të drejtën të respektojmë këto terma dhe kushte
                    dhe po lidh politikën në çdo kohë. Duke u lidhur vazhdimisht me faqen tonë të internetit, ju pranoni
                    të jeni të detyruar dhe të ndiqni këto terma dhe kushte të lidhjes.
                </p>
                <p>
                    Heqja e lidhjeve nga faqja jonë e internetit
                    Nëse gjeni ndonjë lidhje në faqen tonë të internetit që është fyese për çfarëdo arsye, jeni të lirë
                    të na kontaktoni dhe të na informoni në çdo moment. Ne do t'i shqyrtojmë kërkesat për heqjen e
                    lidhjeve, por nuk jemi të detyruar ose t'ju përgjigjemi drejtpërdrejt.
                </p>
                <p>
                    Ne nuk sigurojmë që informacioni në këtë faqe interneti është i saktë, ne nuk garantojmë plotësinë
                    ose saktësinë e tij; as nuk premtojmë të sigurojmë që faqja e internetit të mbetet e disponueshme
                    ose që materiali në faqen e internetit të mbahet i përditësuar.
                </p>
                <p>
                    Mohim përgjegjësie
                    Në masën maksimale të lejuar nga ligji në fuqi, ne përjashtojmë të gjitha përfaqësimet, garancitë
                    dhe kushtet në lidhje me faqen tonë të internetit dhe përdorimin e kësaj faqe interneti. Asgjë në
                    këtë mohim nuk do të:
                </p>
                <p>
                    kufizoni ose përjashtoni përgjegjësinë tonë ose tuaj për vdekje ose lëndim personal;
                    kufizoni ose përjashtoni përgjegjësinë tonë ose tuaj për mashtrim ose keqinterpretim mashtrues;
                    kufizoni ndonjë nga detyrimet tona ose tuaja në çfarëdo mënyre që nuk lejohet sipas ligjit në fuqi;
                    ose
                    përjashtoni ndonjë nga detyrimet tona ose tuajat që nuk mund të përjashtohen sipas ligjit në fuqi.
                    Kufizimet dhe ndalimet e përgjegjësisë të vendosura në këtë seksion dhe gjetkë në këtë mohim: (a) i
                    nënshtrohen paragrafit të mësipërm; dhe (b) qeverisin të gjitha detyrimet që lindin sipas mohimit,
                    duke përfshirë detyrimet që lindin në kontratë, në dëm dhe për shkelje të detyrës ligjore.
                </p>
                <p>
                    Për sa kohë që faqja e internetit dhe informacioni dhe shërbimet në faqen e internetit ofrohen pa
                    pagesë, ne nuk do të jemi përgjegjës për asnjë humbje ose dëmtim të çfarëdo natyre.
                </p>
            </div>
        </div>
        <div class="col-lg-3">
        </div>
    </div>
@endsection
