@extends('layouts.landing-layout', ['class' => 'login-page'])

@section('content')
    <section class="section section-shaped section-lg bg-primary" xmlns:livewire="">
        <div class="container pt-lg-2">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card  shadow border-0">
                        <div class="card-header bg-white text-center">
                            <img src="{{ asset('argon-design-system') }}/img/brand/blue.png" style="width: 100px;"
                                 class="img-fluid">
                        </div>
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Krijo profilin me kredencialet e tua</small>
                            </div>
                           <livewire:register />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
