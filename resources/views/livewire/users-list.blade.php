<div class="m-md-5 mr-3 ml-3 mt-3" xmlns:livewire="">
    <div class="card-header border-0">
        <div class="row">
            <div class="col-6 my-auto">
                <h2 class="mb-0">Perdoruesit</h2>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="id">Numri identifikues</th>
                <th scope="col" class="sort" data-sort="id">Emri</th>
                <th scope="col" class="sort" data-sort="consumer">Email</th>
                <th scope="col" class="sort" data-sort="total">Numri i kontaktit</th>
                <th scope="col" class="sort" data-sort="address">Adresa</th>
                <th scope="col" class="sort" data-sort="address">Qyteti</th>
                <th scope="col" class="sort" data-sort="status">Tipi</th>
                <th scope="col" class="sort" data-sort="status">Verifikuar</th>
                <th scope="col" class="sort" data-sort="status">Data e regjistrimit</th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($users as $user)
                <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['phone_number']}}</td>
                    <td>{{$user['address']}}</td>
                    <td>{{$user['city']}}</td>
                    <td>
                        @if($user->isConsumer())
                            <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <span
                            class="status badge badge-pill badge-info">
                            <div class="m-0 px-2 font-weight-bold">
                                Perdorues
                            </div>
                        </span>
                      </span>
                            @elseif($user->isVendor())
                                <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <span
                            class="status badge badge-pill badge-light">
                            <div class="m-0 px-2 font-weight-bold">
                                Shites
                            </div>
                        </span>
                      </span>
                        @elseif($user->isAdmin())
                            <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <span
                            class="status badge badge-pill badge-dark">
                            <div class="m-0 px-2 font-weight-bold">
                                Admin
                            </div>
                        </span>
                      </span>
                        @endif
                    </td>
                    <td>
                        <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <span
                            class="status badge badge-pill
                                @if(isset($user->email_verified_at))
                                badge-success
@else
                                badge-danger
@endif">
                            <div class="m-0 px-2 font-weight-bold">
                                @if(isset($user->email_verified_at))
                                    PO
                                @else
                                    JO
                                @endif
                            </div>
                        </span>
                      </span>
                    </td>
                    <td>
                        {{$user->created_at}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
