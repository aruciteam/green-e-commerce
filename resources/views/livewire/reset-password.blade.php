<form wire:submit.prevent="resetPassword" action="#" method="POST" role="form text-left"
      xmlns:wire="http://www.w3.org/1999/xhtml">
    <section class="section section-shaped section-lg bg-primary" xmlns:wire="http://www.w3.org/1999/xhtml"
             xmlns:livewire="">
        <div class="container pt-lg-2">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow border-0">
                        <div class="card-header bg-white text-center">
                            <img src="{{ asset('argon-design-system') }}/img/brand/blue.png" style="width: 100px;"
                                 class="img-fluid">
                        </div>
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Vendosni fjalekalimin tuaj te ri.</small>
                            </div>

                            <div class="form-group focused">
                                <div class="input-group input-group-alternative @error('password')border border-danger rounded-3 @enderror">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input wire:model="password" class="form-control" placeholder="Fjalekalimi"
                                           type="password">
                                </div>
                                @error('password')
                                <div class="text-danger text-xs"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="text-center">
                                <button type="submit"
                                        class="btn btn-primary w-100 mt-4 mb-0">Ndrysho fjalekalimin</button>
                            </div>

                            @if ($showSuccessNotification)
                                <div wire:model="showSuccesNotification"
                                     class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                                    <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                                    <span class="alert-text text-white">
                                         Fjalekalimi juaj u ndryshua me sukses.
                                        <a href="{{ route('login') }}">
                                            Tani mund te hyni me kredencialet tuaja.
                                        </a>
                                    </span>
                                </div>
                            @endif

                            @if ($showFailureNotification)
                                <div wire:model="showFailureNotification"
                                     class="mt-3 alert alert-light alert-dismissible fade show" role="alert">
                                    <span class="alert-text">Ju lutem vendosni adresen e sakte te email.</span>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
