<form wire:submit.prevent="recoverPassword" action="#" method="POST" role="form text-left"
      xmlns:wire="http://www.w3.org/1999/xhtml">
    <section class="section section-shaped section-lg bg-primary" xmlns:wire="http://www.w3.org/1999/xhtml"
             xmlns:livewire="">
        <div class="container pt-lg-2">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow border-0">
                        <div class="card-header bg-white text-center">
                            <img src="{{ asset('argon-design-system') }}/img/brand/blue.png" style="width: 100px;"
                                 class="img-fluid">
                        </div>
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Harruat fjalekalimin? Vendosni email-in tuaj me poshte per te ndryshuar fjalekalimin.</small>
                            </div>

                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative @error('email')border border-danger rounded-3 @enderror">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input wire:model="email" class="form-control" placeholder="Email"
                                           type="email">

                                </div>
                                @error('email')
                                <div class="text-danger"><small>{{ $message }}</small></div>
                                @enderror
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4" style="width: 100%">Ndrysho fjalekalimin</button>
                            </div>

                            @if ($showSuccessNotification)
                                <div wire:model="showSuccesNotification"
                                     class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                                    <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                                    <span
                                        class="alert-text text-white">Nje email eshte derguar ne adresen tuaj per te ndryshuar fjalekalimin.</span>
                                </div>
                            @endif

                            @if ($showFailureNotification)
                                <div wire:model="showFailureNotification"
                                     class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                                    <span class="alert-text text-white">
                                        Ti nuk ke nje profil me kete email. Per te krijuar nje profil kliko
                                        <a class="" href="{{ route('register') }}">ketu</a>.</span>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
