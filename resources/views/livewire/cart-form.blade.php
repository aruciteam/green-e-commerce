<form wire:submit.prevent="order" method="POST" role="form" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="form-group mt-3 mb-5">
        <div class="input-group @error('phone')border border-danger rounded-3 @enderror">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-phone"></i></span>
            </div>
            <input wire:model="phone" class="form-control" placeholder="Numri i kontaktit" type="text">
        </div>
        @error('phone')
        <div class="text-danger"><small>{{ $message }}</small></div>
        @enderror
        <div class="row mt-4">
            <div class="col-sm-8 col-12">
                <div class="input-group @error('address')border border-danger rounded-3 @enderror">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                    </div>
                    <input wire:model="address" class="form-control" placeholder="Adresa e dergeses" type="text">
                </div>
                @error('address')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
            <div class="col-sm-4 col-12  mt-sm-0 mt-4">
                <div class="input-group @error('city')border border-danger rounded-3 @enderror">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-map"></i></span>
                    </div>
                    <input wire:model="city" class="form-control" placeholder="Qyteti" type="text">
                </div>
                @error('city')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>

        <div class="">
            <label for="cart-note"></label>
            <textarea wire:model="notes" class="form-control" id="cart-note" rows="3"
                      placeholder="Shenime rreth porosise apo adreses"></textarea>
        </div>
    </div>
    @error('products')
    <div class="text-danger text-right mb-3"><small>{{ $message }}</small></div>
    @enderror
    <div class="text-right">
        @include('components.shopping-cart-buttons')
    </div
</form>
