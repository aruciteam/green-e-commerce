<tr xmlns:wire="http://www.w3.org/1999/xhtml">
    <th scope="row" class="id">
        <a href="{{route('orders-info-page', $order['id'])}}">
            <span class="name mb-0 text-lg"># {{$order['id']}}</span>
        </a>
    </th>
    <td class="budget" style="white-space: pre-wrap;">{{$order['consumer']['name']}}</td>
    <td>
        {{$order['total_price']}} Lek
    </td>
    <td style="white-space: pre-wrap;">{{$order['address']}}, {{$order['city']}}</td>
    <td>
                      <span class="badge badge-dot mr-4">
                        <i class="bg-warning"></i>
                        <span class="status badge badge-pill @if($order->isPending()) badge-danger @endif
                        @if($order->isAccepted()) badge-primary @endif
                        @if($order->isRejected()) badge-dark @endif
                        @if($order->isCompleted()) badge-success @endif">
                            <div class="font-weight-bold">{{$order['status']}}</div>
                        </span>
                      </span>
    </td>
    @if(auth()->user()->isAdmin())
        <td>
            <a href="{{route('store-admin-edit', $order['store']['id'])}}">
                {{$order['store']['name']}}
            </a>
        </td>
    @endif
    <td>
        @if($order->isPending())
            <button wire:click="accept" type="button" class="btn btn-success">Prano</button>
            <button wire:click="reject" type="button" class="btn btn-light">Refuzo</button>
        @endif
        @if($order->isAccepted())
            <button wire:click="complete" type="button" class="btn btn-primary">Perfundo</button>
        @endif
    </td>
</tr>
