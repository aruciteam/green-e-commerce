<div class="m-md-5 mr-3 ml-3 mt-3" xmlns:livewire="">
    <div class="card-header border-0">
        <div class="row">
            <div class="col-6 my-auto">
                <h2 class="mb-0">Kategorite e produkteve</h2>
                <small>(Lloji i kategorise mund te jete: Fruta, Perime, Mish, Bulmet e.t.j)</small>
            </div>
            <div class="col-6 text-right">
                <a href="{{route('category-create')}}" class="btn btn-primary">+ Krijo kategori</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="id">Numri Identifikues</th>
                <th scope="col" class="sort" data-sort="consumer">Emri</th>
                <th scope="col" class="sort" data-sort="total">Pershkrimi</th>
                <th scope="col" class="sort" data-sort="address">Lloji</th>
                <th scope="col" class="sort" data-sort="action">Imazhi</th>
                <th scope="col" class="sort" data-sort="action">Kodi i ngjyres</th>
                <th scope="col" class="sort" data-sort="action">Veprimet</th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($categories as $category)
                <livewire:product-category-list-row :category="$category"/>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
