<form wire:submit.prevent="save" method="POST" role="form" xmlns:wire="http://www.w3.org/1999/xhtml" class="m-md-5 mr-3 ml-3 mt-3">
    <h1 class="mb-5 mt-3">
        @if(isset($productId))
            Produkti #{{$productId}}
        @else
            Krijo nje produkt
        @endif
    </h1>
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="name">
                    Emri
                </label>
                <input wire:model="name" id="name" type="text" placeholder="Emri"
                       class="form-control @error('name')border border-danger rounded-3 @enderror"/>
                @error('name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="unit_name">
                    Emri i njesise
                </label>
                <input wire:model="unit_name" id="unit_name" type="text" placeholder="Emri i njesise"
                       class="form-control @error('unit_name')border border-danger rounded-3 @enderror"/>
                @error('unit_name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="unit_price">
                    Cmimi i njesise
                </label>
                <input wire:model="unit_price" id="unit_price" type="text" placeholder="Cmimi i njesise"
                       class="form-control @error('unit_price')border border-danger rounded-3 @enderror"/>
                @error('unit_price')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-12 col-sm-6">
            <div class="form-group">
                <label for="description">
                    Pershkrimi
                </label>
                <textarea wire:model="description" id="description" type="text" placeholder="Pershkrimi"
                          class="form-control @error('description')border border-danger rounded-3 @enderror"></textarea>
                @error('description')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="mb-4 font-weight-bold">
                Kategoria e produktit
                @error('product_category_id')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-6 col-12">
                        <div wire:click="setCategory({{$category->id}})" class="custom-control custom-radio mb-3">
                            <input name="custom-radio-{{$category->id}}"
                                   class="custom-control-input" id="customRadio-{{$category->id}}"
                                   @if(isset($product_category_id) && $product_category_id === $category->id)
                                   checked
                                   @endif
                                   type="radio">
                            <label class="custom-control-label" for="customRadio-{{$category->id}}">
                                <span>{{$category['name']}}</span>
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12">
            <div class="mb-4 font-weight-bold">
                Imazhi i produktit
                @error('photo')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
            @if(isset($image_url))
                <img class="product-cart-img mb-3"
                     src="{{$image_url}}">
            @endif
            <div class="form-group">
                <input class="form-control-xs" type="file" id="formFile" wire:model="photo">
                @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            @if($photo !== null)
                <img id="frame" src="{{$photo->temporaryUrl()}}"
                     width="200" height="200">
            @endif
        </div>
    </div>
    <button type="submit" class="btn btn-primary mt-4 mb-5">Ruaj te dhenat</button>
</form>
