<form wire:submit.prevent="save" method="POST" role="form" class="m-md-5 mr-3 ml-3 mt-3" xmlns:wire="http://www.w3.org/1999/xhtml">
    <h1 class="mb-4">Krijo nje dyqan</h1>
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="name">
                    Emri i dyqanit
                </label>
                <input wire:model="name" id="name" type="text" placeholder="Emri"
                       class="form-control @error('name')border border-danger rounded-3 @enderror"/>
                @error('name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="name">
                    Nipti i dyqanit
                </label>
                <input wire:model="nipt" id="nipt" type="text" placeholder="Nipti"
                       class="form-control @error('nipt')border border-danger rounded-3 @enderror"/>
                @error('nipt')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>

        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="delivery_price">
                    Kosto e transportit
                </label>
                <input wire:model="delivery_price" id="delivery_price" type="text" placeholder="Kosto e transportit"
                       class="form-control @error('delivery_price')border border-danger rounded-3 @enderror"/>
                @error('delivery_price')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="phone_number">
                    Numer kontakti i dyqanit
                </label>
                <input wire:model="phone_number" id="phone_number" type="text" placeholder="Numer kontakti"
                       class="form-control @error('phone')border border-danger rounded-3 @enderror"/>
                @error('phone_number')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="address">
                    Adresa e dyqanit
                </label>
                <input wire:model="address" id="address" type="text" placeholder="Adresa"
                       class="form-control @error('address')border border-danger rounded-3 @enderror"/>
                @error('address')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-12 col-sm-6">
            <div class="form-group">
                <label for="description">
                    Pershkrimi i dyqanit
                </label>
                <textarea wire:model="description" id="description" type="text" placeholder="Pershkrimi"
                          class="form-control @error('description')border border-danger rounded-3 @enderror"></textarea>
                @error('description')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-12">
            <div class="mb-4 font-weight-bold">
                Imazhi i dyqanit
                @error('photo')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
            @if(isset($image_url))
                <img class="store-img mb-3"
                     src="{{$image_url}}">
            @endif
            <div class="form-group">
                <input class="form-control-xs" type="file" id="formFile" wire:model="photo">
                @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            @if($photo !== null)
                <img id="frame" src="{{$photo->temporaryUrl()}}"
                     width="200" height="200">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="owner_name">
                    Emri i menaxherit
                </label>
                <input wire:model="owner_name" id="owner_name" type="text" placeholder="Emri i menaxherit"
                       class="form-control @error('owner_name')border border-danger rounded-3 @enderror"/>
                @error('owner_name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="email">
                    Email i menaxherit
                </label>
                <input wire:model="email" id="email" type="text" placeholder="Email i menaxherit"
                       class="form-control @error('email')border border-danger rounded-3 @enderror"/>
                @error('email')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="owner_password">
                    Fjalekalimi i menaxherit
                </label>
                <input wire:model="owner_password" id="owner_password" type="password" placeholder="Fjalekalimi i menaxherit"
                       class="form-control @error('owner_password')border border-danger rounded-3 @enderror"/>
                @error('owner_password')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="owner_phone">
                    Numri i kontaktit i menaxherit
                </label>
                <input wire:model="owner_phone" id="owner_phone" type="text" placeholder="Numri i kontaktit i menaxherit"
                       class="form-control @error('owner_phone')border border-danger rounded-3 @enderror"/>
                @error('owner_phone')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="owner_address">
                    Adresa e menaxherit
                </label>
                <input wire:model="owner_address" id="owner_address" type="text" placeholder="Adresa e menaxherit"
                       class="form-control @error('owner_address')border border-danger rounded-3 @enderror"/>
                @error('owner_address')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="owner_city">
                    Qyteti i menaxherit
                </label>
                <input wire:model="owner_city" id="owner_city" type="text" placeholder="Qyteti i menaxherit"
                       class="form-control @error('owner_city')border border-danger rounded-3 @enderror"/>
                @error('owner_city')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
    </div>

@if($success)
        <div class="text-info mb-3">
            Te dhenat u ruajten me sukses.
        </div>
    @endif
    <button type="submit" class="btn btn-primary mb-5">Ruaj te dhenat</button>
</form>
