<form wire:submit.prevent="login" method="POST" role="form" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="form-group mb-3">
        <div class="input-group input-group-alternative @error('email')border border-danger rounded-3 @enderror">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input wire:model="email" class="form-control" placeholder="Email"
                   type="email">

        </div>
        @error('email')
        <div class="text-danger"><small>{{ $message }}</small></div>
        @enderror
    </div>

    <div class="form-group focused">
        <div class="input-group input-group-alternative @error('password')border border-danger rounded-3 @enderror">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input wire:model="password" class="form-control" placeholder="Fjalekalimi"
                   type="password">
        </div>
        @error('password')
        <div class="text-danger text-xs"><small>{{ $message }}</small></div>
        @enderror
    </div>
    <div class="custom-control custom-control-alternative custom-checkbox">
        <input wire:model="remember_me" class="custom-control-input" id=" customCheckLogin" type="checkbox">
        <label class="custom-control-label"
               for=" customCheckLogin"><span>Me mbaj mend</span></label>
    </div>
    <div class="text-right">
        <a href="{{route('forgot-password')}}" class="text-light"><small>Harruat fjalekalimin?</small></a>
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary my-4" style="width: 100%">Hyr</button>
    </div>
    <div class="text-center p-0">
        Ose
    </div>
    <div class="text-center">
        <a class="btn btn-secondary text-white mt-4" href="{{route('register', ['ardhurNgaShporta' => $isFromCart])}}" style="width: 100%">
            Krijo profil
        </a>
    </div>
</form>
