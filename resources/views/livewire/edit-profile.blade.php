<form wire:submit.prevent="update" method="POST" role="form" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="name">
                    Emri
                </label>
                <input wire:model="name" id="name" type="text" placeholder="Emri"
                       class="form-control @error('name')border border-danger rounded-3 @enderror"/>
                @error('name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="email">
                    Email
                </label>
                <input wire:model="email" id="emai" type="email" placeholder="Email" class="form-control" readonly/>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="phone">
                    Numer kontakti
                </label>
                <input wire:model="phone" id="phone" type="text" placeholder="Numer kontakti"
                       class="form-control @error('phone')border border-danger rounded-3 @enderror"/>
                @error('phone')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="address">
                    Adresa
                </label>
                <input wire:model="address" id="address" type="text" placeholder="Adresa"
                       class="form-control @error('address')border border-danger rounded-3 @enderror"/>
                @error('address')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="city">
                    Qyteti
                </label>
                <input wire:model="city" id="city" type="text" placeholder="Qyteti"
                       class="form-control @error('city')border border-danger rounded-3 @enderror"/>
                @error('city')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Ruaj te dhenat</button>
</form>
