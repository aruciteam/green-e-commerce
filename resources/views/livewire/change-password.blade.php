<form wire:submit.prevent="changePassword" method="POST" role="form" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="password">
                    Fjalekalimi aktual
                </label>
                <input wire:model="password" id="password" type="password" placeholder="Fjalekalimi aktual"
                       class="form-control @error('password')border border-danger rounded-3 @enderror"/>
                @error('password')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="new_password">
                    Fjalekalimi i ri
                </label>
                <input wire:model="newPassword" id="new_password" type="password" placeholder="Fjalekalimi i ri"
                       class="form-control @error('newPassword')border border-danger rounded-3 @enderror"/>
                @error('newPassword')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
    </div>
    @if($success)
        <div class="text-success"><small>Fjalekalimi u ndryshua me sukses!</small></div>
    @endif
    <button type="submit" class="btn btn-primary">Ndrysho fjalekalimin</button>
</form>
