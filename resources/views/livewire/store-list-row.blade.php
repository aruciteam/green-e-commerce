<tr xmlns:wire="http://www.w3.org/1999/xhtml">
    @if(isset($store))
        <th scope="row">
            <a href="{{route('store-admin-edit', $store['id'])}}">
                <span class="name mb-0 text-lg"># {{$store['id']}}</span>
            </a>
        </th>
        <td class="font-weight-bold">{{$store['nipt']}}</td>
        <td class="budget" style="white-space: pre-wrap;">{{$store['name']}}</td>
        <td style="white-space: pre-wrap;">{{$store['description']}}</td>
        <td style="white-space: pre-wrap;">{{$store['address']}}</td>
        <td>
            {{$store['phone_number']}}
        </td>
        <td>
            <img class="product-cart-img"
                 src="{{$store['image_url']}}">
        </td>
        <td>
            <a href="{{route('store-products-admin', $store['id'])}}" class="btn btn-info">Produktet</a>
            <a href="{{route('store-admin-edit', $store['id'])}}" class="btn btn-success">Modifiko</a>
            <button wire:click="delete" type="button" class="btn btn-danger">Fshij</button>
        </td>
    @endif
</tr>
