<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <form wire:submit.prevent="register" method="POST" role="form">
        <div class="form-group mb-3">
            <div
                class="input-group input-group-alternative @error('name')border border-danger rounded-3 @enderror">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                </div>
                <input wire:model="name" class="form-control" placeholder="Emri i plote" type="text">
            </div>
            @error('name')
            <div class="text-danger"><small>{{ $message }}</small></div>
            @enderror
        </div>
        <div class="form-group mb-3">
            <div
                class="input-group input-group-alternative @error('email')border border-danger rounded-3 @enderror">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                </div>
                <input wire:model="email" class="form-control" placeholder="Email" type="email">
            </div>
            @error('email')
            <div class="text-danger"><small>{{ $message }}</small></div>
            @enderror
        </div>
        <div class="form-group focused mb-3">
            <div class="input-group input-group-alternative @error('password')border border-danger rounded-3 @enderror">
                <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="ni ni-lock-circle-open"></i></span>
                </div>
                <input wire:model="password" class="form-control" placeholder="Fjalekalimi" type="password">
            </div>
            @error('password')
            <div class="text-danger text-xs"><small>{{ $message }}</small></div>
            @enderror
        </div>
        <div class="row my-4">
            <div class="col-12">
                <div class="custom-control custom-control-alternative custom-checkbox">
                    <input wire:model="acceptedPrivacyPolicy" class="custom-control-input" id="customCheckRegister"
                           type="checkbox">
                    <label class="custom-control-label" for="customCheckRegister"><span>Une jam dakord me <a
                                href="#">Politikat e Privatesise</a></span></label>
                </div>
                @error('acceptedPrivacyPolicy')
                <div class="text-danger text-xs"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary my-4" style="width: 100%">Krijo
                profilin
            </button>
        </div>
        <div class="text-center p-0">
            Ose
        </div>
        <div class="text-center">
            <a href="{{route('login', ['ardhurNgaShporta' => $isFromCart])}}" class="btn btn-secondary my-4 text-white"
                    style="width: 100%">Hyr
            </a>
        </div>
    </form>
</div>
