<div class="m-md-5 mr-3 ml-3 mt-3" xmlns:livewire="">
    <div class="card-header border-0">
        <div class="row">
            <div class="col-6 my-auto">
                <h2 class="mb-0">Produktet</h2>
            </div>
            <div class="col-6 text-right">
                <a href="
                @if(auth()->user()->isVendor())
                {{route('product-create')}}
                @elseif(isset($storeId))
                {{route('product-create-admin', $storeId)}}
                @endif
                    " class="btn btn-primary">+ Krijo produkt</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="id">Numri Identifikues</th>
                <th scope="col" class="sort" data-sort="consumer">Emri</th>
                <th scope="col" class="sort" data-sort="total">Pershkrimi</th>
                <th scope="col" class="sort" data-sort="address">Njesia</th>
                <th scope="col" class="sort" data-sort="address">Cmimi per njesi</th>
                <th scope="col" class="sort" data-sort="status">Kategoria</th>
                <th scope="col" class="sort" data-sort="action">Imazhi</th>
                <th scope="col" class="sort" data-sort="action">Veprimet</th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($products as $product)
                <livewire:product-list-row :product="$product"/>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
