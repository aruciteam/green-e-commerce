<tr xmlns:wire="http://www.w3.org/1999/xhtml">
    @if(isset($category))
        <th scope="row">
            <a href="{{route('category-edit', $category['id'])}}">
                <span class="name mb-0 text-lg"># {{$category['id']}}</span>
            </a>
        </th>
        <td class="budget" style="white-space: pre-wrap;">{{$category['name']}}</td>
        <td style="white-space: pre-wrap;">{{$category['description']}}</td>
        <td>{{$category['type']}}</td>
        <td>
            <img class="product-cart-img"
                 src="{{$category['image_url']}}">
        </td>
        <td>
            {{$category['color']}}
        </td>
        <td>
            <a href="{{route('category-products-admin', $category['id'])}}" class="btn btn-info">Produktet</a>
            <a href="{{route('category-edit', $category['id'])}}" class="btn btn-success">Modifiko</a>
            <button wire:click="delete" type="button" class="btn btn-danger">Fshij</button>
        </td>
    @endif
</tr>
