<form wire:submit.prevent="save" method="POST" role="form" class="m-md-5 mr-3 ml-3 mt-3" xmlns:wire="http://www.w3.org/1999/xhtml">
    @if(isset($isCreateCategory))
        <h1 class="mb-4">Krijo nje kategori</h1>
    @endif
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="name">
                    Emri
                </label>
                <input wire:model="name" id="name" type="text" placeholder="Emri"
                       class="form-control @error('name')border border-danger rounded-3 @enderror"/>
                @error('name')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>

        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="type">
                    Lloji
                </label>
                <input wire:model="type" id="type" type="text" placeholder="Lloji"
                       class="form-control @error('type')border border-danger rounded-3 @enderror"/>
                @error('type')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="form-group">
                <label for="color">
                    Ngjyra
                </label>
                <input wire:model="color" id="color" type="text" placeholder="Ngjyra"
                       class="form-control @error('color')border border-danger rounded-3 @enderror"/>
                @error('color')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-lg-12 col-sm-6">
            <div class="form-group">
                <label for="description">
                    Pershkrimi
                </label>
                <textarea wire:model="description" id="description" type="text" placeholder="Pershkrimi"
                          class="form-control @error('description')border border-danger rounded-3 @enderror"></textarea>
                @error('description')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <div class="col-12">
            <div class="mb-4 font-weight-bold">
                Imazhi i kategories
                @error('photo')
                <div class="text-danger"><small>{{ $message }}</small></div>
                @enderror
            </div>
            @if(isset($image_url))
                <img class="product-cart-img mb-3"
                     src="{{$image_url}}">
            @endif
            <div class="form-group">
                <input class="form-control-xs" type="file" id="formFile" wire:model="photo">
                @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            @if($photo !== null)
                <img id="frame" src="{{$photo->temporaryUrl()}}"
                     width="200" height="200">
            @endif
        </div>
    </div>
    @if($success)
        <div class="text-info mb-3">
            Te dhenat u ruajten me sukses.
        </div>
    @endif
    <button type="submit" class="btn btn-primary mt-4 mb-5">Ruaj te dhenat</button>
</form>
