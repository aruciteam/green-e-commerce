<div class="m-md-5 mr-3 ml-3 mt-3" xmlns:livewire="">
    <div class="card-header border-0">
        <div class="row">
            <div class="col-6 my-auto">
                <h2 class="mb-0">Dyqanet</h2>
            </div>
            <div class="col-6 text-right">
                <a href="{{route('store-create')}}" class="btn btn-primary">+ Krijo dyqan</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="id">Numri Identifikues</th>
                <th scope="col" class="sort" data-sort="status">Nipt</th>
                <th scope="col" class="sort" data-sort="consumer">Emri</th>
                <th scope="col" class="sort" data-sort="total">Pershkrimi</th>
                <th scope="col" class="sort" data-sort="address">Adresa</th>
                <th scope="col" class="sort" data-sort="address">Numri i kontaktit</th>
                <th scope="col" class="sort" data-sort="action">Imazhi</th>
                <th scope="col" class="sort" data-sort="action">Veprimet</th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($stores as $store)
                <livewire:store-list-row :store="$store"/>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
