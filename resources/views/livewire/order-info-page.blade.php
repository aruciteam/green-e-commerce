<div class="m-md-5 ml-3 mr-3" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="col-lg-6 mt-lg-0 mt-4">
        <h1 class="modal-title mb-3 " id="modal-title-default">Porosia #{{$order->id}}</h1>
        <span class="badge badge-dot mr-4 mb-2 mt-2">
                        <i class="bg-warning"></i>
                        <span class="status badge badge-pill @if($order->isPending()) badge-danger @endif
                        @if($order->isAccepted()) badge-primary @endif
                        @if($order->isRejected()) badge-dark @endif
                        @if($order->isCompleted()) badge-success @endif">
                           <p class="m-0 px-2 font-weight-bold">{{$order['status']}}</p>
                        </span>
        </span>
        @if($order->isPending())
            <button wire:click="accept" type="button" class="btn btn-success">Prano</button>
            <button wire:click="reject" type="button" class="btn btn-light">Refuzo</button>
        @endif
        @if($order->isAccepted())
            <button wire:click="complete" type="button" class="btn btn-primary">Perfundo</button>
        @endif
        @if(auth()->user()->isAdmin())
            <div class="mt-2">
                <a href="{{route('store-admin-edit', $order['store']['id'])}}" >
                    <p class="font-weight-bold text-lg">Dyqani: {{$order->store->name}}</p>
                </a>
            </div>
        @endif
        <div>
            @foreach($order->products as $product)
                <div class="row mt-3">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-3 col-5">
                        <img class="product-cart-img"
                             src="{{$product['image_url']}}">
                    </div>
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-9 col-7 my-auto">
                        <div class="product-cart-name">
                            {{$product['name']}}
                        </div>
                        <div>
                            <span id="qnt-cart-1">{{$product['quantity']}}</span>
                            x {{$product['unit_price']}} Lek
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mt-5">
            Cmimi i dergeses: {{$order['delivery_price']}} Lek
            <h2 class="cart-total">
                Total: {{$order['total_price']}} Lek
            </h2>
        </div>
        <hr>
        <div>
            Emri i klientit: {{$order['consumer']['name']}}
        </div>
        <div class="mb-3">
            Numri i klientit: {{$order['consumer']['phone_number']}}
        </div>
        <div class="mb-3">
            Email i klientit: {{$order['consumer']['email']}}
        </div>
        <div>
            Adresa e dergeses: {{$order['address']}}
        </div>
        <div class="mb-3">
            Qyteti: {{$order['city']}}
        </div>
        <div>
            {{$order['note']}}
        </div>

    </div>
</div>
