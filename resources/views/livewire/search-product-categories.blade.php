<div>
    <div class="navbar-search navbar-search-dark mb-0">
        <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <label for="search-product-categories">
                </label>
                <input wire:model.debounce.200ms="query"
                       id="search-product-categories" class="form-control"
                       placeholder="Kerko produkte bio" type="text">
            </div>
        </div>
        @if(!empty($query))
            <div class="container p-0 rounded bg-white mt-2">
                <ul class="list-group">
                    @if(!empty($categories))
                        @foreach($categories as $i => $category)
                            <a href="{{route('category', $category['id'])}}">
                                <li class="list-group-item" style="cursor: pointer;">{{$category['name']}}</li>
                            </a>
                        @endforeach
                    @else
                        <li class="list-group-item">Nuk ka produkte te tilla.</li>
                    @endif
                </ul>
            </div>
        @endif
    </div>
</div>
