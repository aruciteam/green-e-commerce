<div>
    @if($show)
        <div class="row mb-4" xmlns:wire="http://www.w3.org/1999/xhtml">
            <div class="col-md-2 col-sm-3 col-4">
                <img class="product-cart-img"
                     src="{{$product['imageUrl']}}">
            </div>
            <div class="col-md-8 col-sm-7 col-5 my-auto">
                <div class="product-cart-name">
                    {{$product['name']}}
                </div>
                <div>
                    <span id="qnt-cart-1">{{$quantity}}</span>
                    x {{$product['price']}} Lek
                </div>
{{--                <div class="row col-lg-4 col-md-6 col-sm-8 col-12 mt-2 p-0 justify-content-center">--}}
{{--                    <div wire:click="subtractQuantity" class="quantity-button-small col-3 minus minus-cart-1">-</div>--}}
{{--                    <input wire:model="quantity" type="numeric" min="1"--}}
{{--                           class="quantity-input-small quantity-input-cart-1 col-4"/>--}}
{{--                    <span wire:click="addQuantity" class="plus quantity-button-small col-3 plus-cart-1">+</span>--}}
{{--                </div>--}}
            </div>
            <div class="col-sm-2 col-3 my-auto text-right">
                <button wire:click="removeProduct" type="button" class="btn btn-secondary">
                    <i class="fas fa-trash-alt text-white"></i>
                </button>
            </div>
        </div>
    @endif
</div>
