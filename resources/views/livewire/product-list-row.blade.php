<tr xmlns:wire="http://www.w3.org/1999/xhtml">
    @if(isset($product))
        <th scope="row">
            <a href="{{route('product-save', $product['id'])}}">
                <span class="name mb-0 text-lg"># {{$product['id']}}</span>
            </a>
        </th>
        <td class="budget" style="white-space: pre-wrap;">{{$product['name']}}</td>
        <td style="white-space: pre-wrap;">{{$product['description']}}</td>
        <td>
            {{$product['unit_name']}}
        </td>
        <td>
            {{$product['unit_price']}} Lek
        </td>
        <td style="white-space: pre-wrap;">@if(isset($product['productCategory'])){{$product['productCategory']['name']}}@endif
        </td>
        <td>
            <img class="product-cart-img"
                 src="{{$product['image_url']}}">
        </td>
        <td>
            <a href="{{route('product-save', ['id' => $product['id'], 'fromCategory' => $fromCategory] )}}"
               class="btn btn-success">Modifiko</a>
            <button wire:click="delete" type="button" class="btn btn-danger">Fshij</button>
        </td>
    @endif
</tr>
