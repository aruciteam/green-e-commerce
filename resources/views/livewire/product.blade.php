<div class="product-container pt-4 pb-4 mt-4" style="width: 100%" xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="text-center">
        <img class="product-category-img"
             src="{{$product['image_url']}}">
    </div>
    <div class="mr-4 ml-4 mt-3">
        @if(isset($product['product_category']))
            <p class="product-type-tag" style="color: {{$product['product_category']['color']}}">
                {{$product['store']['name']}} · {{$product['product_category']['type']}}
            </p>
        @else
            <p class="product-type-tag">
                {{$product['store']['name']}}
            </p>
        @endif
        <p class="product-title">
            {{$product['name']}}
        </p>
        <p class="product-unit-text">Njesia: <span class="text-black">{{$product['unit_name']}}</span></p>
        <p class="product-description">
            {{$product['description']}}
        </p>
        <p class="product-price">
            {{$product['unit_price']}} Lek
        </p>
        @if(isset($canBeAddedToCart) &&  $canBeAddedToCart)
            <div class="row mb-3 p-0 justify-content-center">
                <div wire:click="subtractQuantity" class="quantity-button col-3 minus">-</div>
                <input wire:model="quantity" min="1" type="numeric" class="quantity-input col-4"/>
                <span wire:click="addQuantity" class="plus quantity-button col-3">+</span>
            </div>
        @endif

        @if(isset($canBeAddedToCart) &&  $canBeAddedToCart)
            <button type="button" wire:click="addToCart" onclick="showAlert()"
                    class="btn btn-block btn-primary p-3 m-0 border-radius-12">
                Shto ne shporte - {{$product['unit_price'] * $quantity}} Lek
            </button>
        @else
            <a href="{{route('store', $product['store']['id'])}}"
               class="btn btn-block btn-primary p-3 m-0 border-radius-12">
                Shih dyqanin
            </a>
        @endif
    </div>

    <div class="alert alert-success alert-dismissible d-none position-fixed bottom-1 left-4 " role="alert">
        <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
        <span
            class="alert-inner--text">Produkti u shtua ne shporte.
        </span>
    </div>
</div>

@push('js')
    <script>
        function showAlert() {
            $(".alert").removeClass("d-none")

            setTimeout(function () {
                $(".alert").addClass("d-none")
            }, 1500)
        }
    </script>
@endpush
