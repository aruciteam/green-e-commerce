@include('components.order-info-product', ['products' => $order['products']])
<div class="mt-5">
    Cmimi i dergeses: {{$order['delivery_price']}} Lek
    <h4 class="cart-total">
        Total: {{$order['total_price']}} Lek
    </h4>
</div>
<hr>
<div>
    Numri juaj: {{$order['consumer']['phone_number']}}
</div>
<div>
    Adresa e dergeses: {{$order['address']}}
</div>
<div>
    Qyteti: {{$order['city']}}
</div>
<div>
    {{$order['note']}}
</div>
