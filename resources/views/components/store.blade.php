<div class="mb-5">
    <a href="{{route('store', $store['id'])}}">
        <img class="store-container-img mb-2"
             src="{{$store['image_url']}}">
        <p class="store-container-title">
            {{$store['name']}}
        </p>
        <p class="store-container-desc">
            {{$store['description']}}
        </p>
        <p class="store-container-desc">
            Transporti: {{$store['delivery_price']}} Lek
        </p>
        <p class="store-container-desc">
            Adresa: {{$store['address']}}
        </p>
    </a>
</div>
