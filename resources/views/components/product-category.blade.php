<a href="{{route('category', $category['id'])}}">
    <div class="product-category-container text-center mt-4" style="background-color: {{$category['color']}};">
        <p class="product-category-title">
            {{$category['name']}}
        </p>
        <img class="product-category-img"
             src="{{$category['image_url']}}">
    </div>
</a>
