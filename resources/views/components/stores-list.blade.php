<div class="row justify-content-center">
    <div class="col-lg-12">
        <!-- Basic elements -->
        <h2 class="mb-5">
            <span class="font-weight-bold text-secondary">Dyqanet</span>
            <div class="row mt-4">
                @foreach($stores as $i => $store)
                    <div class="col-xl-4 col-sm-6 col-12">
                        @include('components.store', ['store' => $store])
                    </div>
                @endforeach

            </div>
        </h2>
    </div>
</div>
