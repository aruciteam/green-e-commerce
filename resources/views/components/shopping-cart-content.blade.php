<div xmlns:livewire="">
    @foreach($products as $i => $product)
        @if($i !== 'delivery')
            <livewire:shopping-cart-product-component :product="$product"/>
        @endif
    @endforeach()
    <div class="mt-5">
        @if(isset($products['delivery'] ))
            Cmimi i dergeses: {{$products['delivery']['price']}} Lek
            <br>
            <small class="font-italic">
                * Mund te ndryshoje ne varesi te distances. Ju do
                te telefonoheni per cdo ndryshim ne koston e transportit.
            </small>
        @endif
        <h4 class="cart-total">
            Total: {{$total}} Lek
        </h4>
    </div>

</div>
