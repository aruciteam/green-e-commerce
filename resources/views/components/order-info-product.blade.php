<div>
    @foreach($products as $product)
        <div class="row mt-3">
            <div class="col-xl-2 col-lg-3 col-md-2 col-sm-3 col-5">
                <img class="product-cart-img"
                     src="{{$product['image_url']}}">
            </div>
            <div class="col-xl-10 col-lg-9 col-md-10 col-sm-9 col-7 my-auto">
                <div class="product-cart-name">
                    {{$product['name']}}
                </div>
                <div>
                    <span id="qnt-cart-1">{{$product['quantity']}}</span>
                    x {{$product['unit_price']}} Lek
                </div>
            </div>
        </div>
    @endforeach
</div>

