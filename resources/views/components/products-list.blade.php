<div class="row justify-content-center" xmlns:livewire="">
    <div class="col-lg-12">
        <!-- Basic elements -->
        <h2 class="mb-5">
            <span class="font-weight-bold text-secondary">Produktet</span>
            <div class="row mt-4">
                @foreach($products as $i => $product)
                    <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                        <livewire:product-component :product="$product" :canBeAddedToCart="$canBeAddedToCart"/>
                    </div>
                @endforeach
            </div>
        </h2>
    </div>
</div>
