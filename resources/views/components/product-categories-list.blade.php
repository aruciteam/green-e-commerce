<div class="row justify-content-center">
    <div class="col-lg-12">
        <!-- Basic elements -->
        <h2 class="mb-5">
            <span class="font-weight-bold text-secondary">Kategorite e produkteve</span>
            <div class="row mt-4">
                @foreach($categories as $i => $category)
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12">
                    @include('components.product-category', ['category' => $category])
                </div>
                @endforeach
            </div>
        </h2>
    </div>
</div>
