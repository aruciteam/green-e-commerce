<footer class="">
    <div class="m-md-5 m-3">
        <hr>
        <div class="row align-items-center justify-content-md-between">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <ul class="nav nav-footer justify-content-center">
                    <li class="nav-item">
                        <a href="{{route('partners')}}" class="nav-link pt-2" >Partneret</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('contact-us')}}" class="nav-link pt-2" >Kontakt</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('privacy-policy')}}" class="nav-link pt-2" >Kushtet e privatesise</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('terms-and-services')}}" class="nav-link pt-2" >Termat e perdorimit</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="text-center mt-4 mb-2">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img mr-2" alt="..." style="height: 50px;">
            <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="ml-2" style="height: 50px;">
        </div>
    </div>
</footer>
