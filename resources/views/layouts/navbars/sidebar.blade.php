<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main"
     xmlns:livewire="">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
                aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('orders') }}">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img mr-2" alt="...">
            <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="ml-2">
        </a>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('orders') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                            <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="ml-2">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                                aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('orders') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Porosite') }}
                    </a>
                </li>
                @if(auth()->user()->isVendor())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('store-products') }}">
                            <i class="ni ni-book-bookmark text-blue"></i> {{ __('Produktet') }}
                        </a>
                    </li>
                @elseif(auth()->user()->isAdmin())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('stores-list') }}">
                            <i class="ni ni-app text-blue"></i> {{ __('Dyqanet') }}
                        </a>
                    </li>
                @endif
                @if(auth()->user()->isVendor())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('store-edit') }}">
                            <i class="ni ni-app text-blue"></i> {{ __('Dyqani') }}
                        </a>
                    </li>
                @elseif(auth()->user()->isAdmin())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('categories-list')}}">
                            <i class="ni ni-book-bookmark text-blue"></i> {{ __('Kategorite') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users-list')}}">
                            <i class="ni ni-single-02 text-blue"></i> {{ __('Perdoruesit') }}
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile')}}">
                        <i class="ni ni-circle-08 text-pink"></i> {{ __('Profili') }}
                    </a>
                </li>
                <hr/>
                <li class="nav-item border-top border-bottom">
                    <div class="nav-link">
                        <i class="ni ni-user-run text-pink"></i>
                        <livewire:logout/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
