<nav id="navbar-main"
     class="navbar navbar-main navbar-expand-lg bg-white navbar-light position-sticky top-0 shadow py-2"
     xmlns:livewire="">
    <div class="container">
        <a class="navbar-brand mr-lg-5" href="{{route('index')}}">
            <img src="{{ asset('argon-design-system') }}/img/brand/blue.png" class="mr-2">
            <img src="{{ asset('assets') }}/img/bashkia_logo.png" class="ml-2">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global"
                aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse pb-2 pt-2" id="navbar_global">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{route('index')}}">
                            <img src="{{ asset('argon-design-system') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global"
                                aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                @guest()
                    <li class="nav-item mt-2">
                        <a class="btn btn-outline-primary" href="{{route('login')}}"
                        >
                            <span class="nav-link-inner--text">Hyr</span>
                        </a>
                    </li>
                @endguest
                @auth()
                    <li class="nav-item mt-2">
                        <a href="{{route('profile')}}"
                           class="btn btn-outline-primary btn-icon">
                          <span class="btn-inner--icon">
                            <i class="fa fa-user"></i>
                          </span>
                            <span class="nav-link-inner--text">Profili</span>
                        </a>
                    </li>
                @endauth
                <li class="nav-item mt-2">
                    <a href="{{route('cart')}}"
                       class="btn btn-primary btn-icon"
                        {{--                       data-toggle="modal" data-target="#shopping-cart-modal"--}}
                    >
                          <span class="btn-inner--icon">
                            <i class="fa fa-shopping-cart"></i>
                          </span>
                        <span class="nav-link-inner--text">Shporta</span>
                    </a>
                </li>
                @auth()
                        <li class="nav-item mt-2">
                            <livewire:logout />
                        </li>
                @endauth()

            </ul>
        </div>
    </div>
</nav>
