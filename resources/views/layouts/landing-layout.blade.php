<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('argon-design-system') }}/img/apple-icon.png">
    <link href="{{ asset('assets') }}/img/brand/blue.ico" rel="icon" type="image/png">
    <title>
        Ferma online
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="{{ asset('argon-design-system') }}/css/nucleo-icons.css" rel="stylesheet"/>
    <link href="{{ asset('argon-design-system') }}/css/nucleo-svg.css" rel="stylesheet"/>
    <!-- Font Awesome Icons -->
    <link href="{{ asset('argon-design-system') }}/css/font-awesome.css" rel="stylesheet"/>
    <link href="{{ asset('argon-design-system') }}/css/nucleo-svg.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="{{ asset('argon-design-system') }}/css/argon-design-system.css?v=1.2.2" rel="stylesheet"/>
    <link href="{{ asset('assets') }}/css/app.css" rel="stylesheet"/>

    @livewireStyles
</head>
<body class="{{ $class ?? 'index-page' }}">
@include('components.shopping-cart-modal')

<div class="main-content">
    @include('layouts.navbars.landing-navbar')
    @yield('content')
</div>

@include('layouts.footers.landing-footer')

<!--   Core JS Files   -->
<script src="{{ asset('argon-design-system') }}/js/core/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('argon-design-system') }}/js/core/popper.min.js" type="text/javascript"></script>
<script src="{{ asset('argon-design-system') }}/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('argon-design-system') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ asset('argon-design-system') }}/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset('argon-design-system') }}/js/plugins/nouislider.min.js" type="text/javascript"></script>
<script src="{{ asset('argon-design-system') }}/js/plugins/moment.min.js"></script>
<script src="{{ asset('argon-design-system') }}/js/plugins/datetimepicker.js" type="text/javascript"></script>
<script src="{{ asset('argon-design-system') }}/js/plugins/bootstrap-datepicker.min.js"></script>
<!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

@stack('js')
<script src="{{ asset('argon-design-system') }}/js/argon-design-system.min.js?v=1.2.2" type="text/javascript"></script>
<script>
    function scrollToDownload() {

        if ($('.section-download').length != 0) {
            $("html, body").animate({
                scrollTop: $('.section-download').offset().top
            }, 1000);
        }
    }
</script>
{{--<script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>--}}
{{--<script>--}}
{{--    window.TrackJS &&--}}
{{--    TrackJS.install({--}}
{{--        token: "ee6fab19c5a04ac1a32a645abde4613a",--}}
{{--        application: "argon-design-system-pro"--}}
{{--    });--}}
{{--</script>--}}


@livewireScripts
</body>
</html>
