<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Banane',
            'description' => 'Banane',
            'unit_name' => 'Kg',
            'unit_price' => 140,
            'store_id' => 1,
            'product_category_id' => 1,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-2.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Karrote',
            'description' => 'Karrote',
            'unit_name' => 'Kg',
            'unit_price' => 90,
            'store_id' => 1,
            'product_category_id' => 2,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-1.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Kastravec',
            'description' => 'Kastravec',
            'unit_name' => 'Kg',
            'unit_price' => 70,
            'store_id' => 2,
            'product_category_id' => 3,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/55.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Limon',
            'description' => 'Limon',
            'unit_name' => 'Kg',
            'unit_price' => 200,
            'store_id' => 2,
            'product_category_id' => 4,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/53.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Kungull',
            'description' => 'Kungull',
            'unit_name' => 'Kg',
            'unit_price' => 350,
            'store_id' => 3,
            'product_category_id' => 5,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/56.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Mish',
            'description' => 'Mish',
            'unit_name' => 'Kg',
            'unit_price' => 900,
            'store_id' => 3,
            'product_category_id' => 6,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-3.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Djath',
            'description' => 'Djath',
            'unit_name' => 'Kg',
            'unit_price' => 750,
            'store_id' => 1,
            'product_category_id' => 7,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-4.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Arra',
            'unit_name' => 'Kg',
            'unit_price' => 650,
            'store_id' => 2,
            'product_category_id' => 9,
            'description' => 'Arra',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-6.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Molle',
            'unit_name' => 'Kg',
            'unit_price' => 200,
            'store_id' => 1,
            'product_category_id' => 8,
            'description' => 'Molle',
            'image_url' => 'https://cdn.picpng.com/apple/apple-view-25231.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Mandarina',
            'unit_name' => 'Kg',
            'unit_price' => 130,
            'store_id' => 1,
            'product_category_id' => 10,
            'description' => 'Mandarina',
            'image_url' => 'https://purepng.com/public/uploads/large/purepng.com-orangeorangefruitfoodtastydeliciousorangecolor-331522582483ulajt.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Piper i zi',
            'unit_name' => 'Kg',
            'unit_price' => 600,
            'store_id' => 1,
            'product_category_id' => 11,
            'description' => 'Piper i zi',
            'image_url' => 'http://pngimg.com/uploads/black_pepper/black_pepper_PNG24.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Sallate jeshile',
            'unit_name' => 'Kg',
            'unit_price' => 80,
            'store_id' => 2,
            'product_category_id' => 10001,
            'description' => 'Sallate jeshile',
            'image_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYI9drWbl9vazZr6XbDtoZ4FmBBbKKLfYolfe8UmLVxCWnv9kD8N484WC81U4uRzYzNE4&usqp=CAU',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Banane e fresket',
            'description' => 'Banane e fresket',
            'unit_name' => 'Kg',
            'unit_price' => 90,
            'store_id' => 2,
            'product_category_id' => 1,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-2.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Karrote mesdhetare',
            'description' => 'Karrote mesdhetare',
            'unit_name' => 'Kg',
            'unit_price' => 160,
            'store_id' => 2,
            'product_category_id' => 2,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-1.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Kastravec greqie',
            'description' => 'Kastravec greqie',
            'unit_name' => 'Kg',
            'unit_price' => 200,
            'store_id' => 1,
            'product_category_id' => 3,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/55.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Limon greqie',
            'description' => 'Limon greqie',
            'unit_name' => 'Kg',
            'unit_price' => 350,
            'store_id' => 1,
            'product_category_id' => 4,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/53.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Kungull',
            'description' => 'Kungull',
            'unit_name' => 'Kg',
            'unit_price' => 320,
            'store_id' => 1,
            'product_category_id' => 5,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/56.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Mish qingji',
            'description' => 'Mish qingji',
            'unit_name' => 'Kg',
            'unit_price' => 950,
            'store_id' => 1,
            'product_category_id' => 6,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-3.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Mish vici',
            'description' => 'Mish vici',
            'unit_name' => 'Kg',
            'unit_price' => 800,
            'store_id' => 2,
            'product_category_id' => 6,
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-3.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
