<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role' => User::$ADMIN,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Owner 1',
            'email' => 'owner_1@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role' => User::$VENDOR,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'name' => 'Owner 2',
            'email' => 'owner_2@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role' => User::$VENDOR,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'id' => 4,
            'name' => 'Owner 3',
            'email' => 'owner_3@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role' => User::$VENDOR,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'id' => 5,
            'name' => 'User 1',
            'email' => 'user_1@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role' => User::$CONSUMER,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
