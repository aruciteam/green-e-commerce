<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('product_categories')->insert([
            'id' => 1,
            'name' => 'Banane',
            'description' => 'Banane',
            'type' => 'Fruta',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-2.png',
            'color' => '#eed352',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 2,
            'name' => 'Karrote',
            'description' => 'Karrote',
            'type' => 'Perime',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-1.png',
            'color' => '#ebb26c',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 3,
            'name' => 'Kastravec',
            'description' => 'Kastravec',
            'type' => 'Perime',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/55.png',
            'color' => '#79e095',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 4,
            'name' => 'Limon',
            'description' => 'Limon',
            'type' => 'Fruta',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/53.png',
            'color' => '#d4d9ad',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 5,
            'name' => 'Kungull',
            'description' => 'Kungull',
            'type' => 'Perime',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/56.png',
            'color' => '#d9bead',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 6,
            'name' => 'Mish',
            'description' => 'Mish',
            'type' => 'Produkte mishi',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-3.png',
            'color' => '#d9adba',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 7,
            'name' => 'Djath',
            'description' => 'Djath',
            'type' => 'Produkte te qumeshtit',
            'image_url' => 'https://demo2wpopal.b-cdn.net/ecolive/wp-content/uploads/2021/10/h2_cte-4.png',
            'color' => '#9dde8a',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 8,
            'name' => 'Molle',
            'description' => 'Molle',
            'type' => 'Fruta',
            'image_url' => 'https://cdn.picpng.com/apple/apple-view-25231.png',
            'color' => '#e6a5a1',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 9,
            'name' => 'Fruta te thata',
            'description' => 'Fruta te thata',
            'type' => 'Fruta te thata',
            'image_url' => 'https://purepng.com/public/uploads/large/purepng.com-mixed-nutsfood-nuts-941524642084jlovi.png',
            'color' => '#bdb1b1',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 10,
            'name' => 'Mandarina',
            'description' => 'Mandarina',
            'type' => 'Fruta',
            'image_url' => 'https://purepng.com/public/uploads/large/purepng.com-orangeorangefruitfoodtastydeliciousorangecolor-331522582483ulajt.png',
            'color' => '#dbc7b4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 11,
            'name' => 'Ereza',
            'description' => 'Ereza',
            'type' => 'Ereza',
            'image_url' => 'http://pngimg.com/uploads/black_pepper/black_pepper_PNG24.png',
            'color' => '#b0aca9',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('product_categories')->insert([
            'id' => 10001,
            'name' => 'Te tjera',
            'description' => 'Kategori te tjera produktesh',
            'type' => 'Te tjera',
            'image_url' => 'https://icon-library.com/images/more-icon-png/more-icon-png-2.jpg',
            'color' => '#deceb7',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
