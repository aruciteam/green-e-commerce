<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
            'id' => 1,
            'name' => 'Dyqan London',
            'description' => 'Perfitoni kualitetin me te mire per cmimin me ekonomik ne treg.',
            'delivery_price' => 100,
            'address' => 'Rruga Tafaj, Fier',
            'phone_number' => '+355699832780',
            'logo_url' => '',
            'image_url' => 'https://images.unsplash.com/photo-1597362925123-77861d3fbac7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80',
            'owner_id' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('stores')->insert([
            'id' => 2,
            'name' => 'Shitje me shumice',
            'description' => 'Shitje te produkteve ushqimore me shumice dhe pakice per te gjithe klientet.',
            'delivery_price' => 0,
            'address' => 'Rruga Qemal Stafa, Fier',
            'phone_number' => '+355699832780',
            'logo_url' => '',
            'image_url' => 'https://images.unsplash.com/photo-1575218823251-f9d243b6f720?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80',
            'owner_id' => 3,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('stores')->insert([
            'id' => 3,
            'name' => 'Arben Ndreu Store',
            'description' => 'Ne kete dyqan mund te gjeni produktet me te mira te Fierit.',
            'delivery_price' => 50,
            'address' => 'Rruga Ekrem Qabej, Fier',
            'phone_number' => '+355699832780',
            'logo_url' => '',
            'image_url' => 'https://images.unsplash.com/photo-1518843875459-f738682238a6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1442&q=80',
            'owner_id' => 4,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
