<?php

use App\Http\Livewire\ForgotPassword;
use App\Http\Livewire\CartComponent;
use App\Http\Livewire\CategoryProductsAdmin;
use App\Http\Livewire\CreateProductCategory;
use App\Http\Livewire\CreateStore;
use App\Http\Livewire\EditStore;
use App\Http\Livewire\Index;
use App\Http\Livewire\Logout;
use App\Http\Livewire\OrderCompletion;
use App\Http\Livewire\OrderInfoPage;
use App\Http\Livewire\ProductCategories;
use App\Http\Livewire\ProductCategoriesPage;
use App\Http\Livewire\ProductCategoryEdit;
use App\Http\Livewire\ProductCreate;
use App\Http\Livewire\ProductCreateAdmin;
use App\Http\Livewire\ProductSave;
use App\Http\Livewire\Profile;
use App\Http\Livewire\ResetPassword;
use App\Http\Livewire\StoreAdminEdit;
use App\Http\Livewire\StoreComponent;
use App\Http\Livewire\StoreOrders;
use App\Http\Livewire\StoreProducts;
use App\Http\Livewire\StoreProductsAdmin;
use App\Http\Livewire\StoresListPage;
use App\Http\Livewire\UsersList;
use App\Http\Livewire\VerifyEmailComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Index::class)->name('index');
Route::get('/dyqan/{id}', StoreComponent::class)->name('store');
Route::get('/kategori/{id}', ProductCategories::class)->name('category');
Route::get('/shporta', CartComponent::class)->name('cart');

Route::get('/hyr', function () {
    return view('landing/login');
})->name('login');
Route::get('/regjistrohu', function () {
    return view('landing/register');
})->name('register');
Route::get('/verifikohu', VerifyEmailComponent::class)->name('email.verify');
Route::get('/harrova-fjalekalimin', ForgotPassword::class)->name('forgot-password');
Route::get('/ndrysho-fjalekalimin', ResetPassword::class)->name('reset-password');


Route::get('/partneret', function () {
    return view('landing/partners');
})->name('partners');
Route::get('/kontakt', function () {
    return view('landing/contact-us');
})->name('contact-us');
Route::get('/kushtet-e-privatesise', function () {
    return view('landing/privacy-policy');
})->name('privacy-policy');
Route::get('/termat-e-perdorimit', function () {
    return view('landing/terms-and-services');
})->name('terms-and-services');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profili', Profile::class)->name('profile');
    Route::get('/dergesa/{id}', OrderCompletion::class)->name('order-completion')->middleware('role:consumer');

    Route::get('/porosite', StoreOrders::class)->name('orders')->middleware('role:[admin,vendor]');
    Route::get('/porosite/{id}', OrderInfoPage::class)->name('orders-info-page')->middleware('role:[admin,vendor]');
    Route::get('/produktet', StoreProducts::class)->name('store-products')->middleware('role:vendor');
    Route::get('/krijo-produkt', ProductCreate::class)->name('product-create')->middleware('role:vendor');
    Route::get('/produktet/{id}', ProductSave::class)->name('product-save')->middleware('role:[admin,vendor]');
    Route::get('/dyqani', EditStore::class)->name('store-edit')->middleware('role:vendor');

    Route::get('/dyqanet', StoresListPage::class)->name('stores-list')->middleware('role:admin');
    Route::get('/krijo-dyqan', CreateStore::class)->name('store-create')->middleware('role:admin');
    Route::get('/dyqani-info/{id}', StoreAdminEdit::class)->name('store-admin-edit')->middleware('role:admin');
    Route::get('/kategorite', ProductCategoriesPage::class)->name('categories-list')->middleware('role:admin');
    Route::get('/kategorite/{id}', ProductCategoryEdit::class)->name('category-edit')->middleware('role:admin');
    Route::get('/krijo-kategori', CreateProductCategory::class)->name('category-create')->middleware('role:admin');
    Route::get('/perdoruesit', UsersList::class)->name('users-list')->middleware('role:admin');
    Route::get('/produktet-e-dyqanit/{id}', StoreProductsAdmin::class)->name('store-products-admin')->middleware('role:admin');
    Route::get('/krijo-produkt/{id}', ProductCreateAdmin::class)->name('product-create-admin')->middleware('role:admin');
    Route::get('/produktet-e-kategorise/{id}', CategoryProductsAdmin::class)->name('category-products-admin')->middleware('role:admin');
});
