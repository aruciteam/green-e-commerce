<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class UsersList extends Component
{
    public $users;

    public function mount(): void
    {
        $this->users = User::all()->sortByDesc('id');
    }

    public function render()
    {
        return view('livewire.users-list')
            ->extends('layouts.app');
    }
}
