<?php


namespace App\Http\Livewire;


use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Store;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Index extends Component
{

    public $categories;
    public $stores;
    public $products;

    public function mount(): void
    {
        $this->categories = ProductCategory::with('products', 'products.store')
            ->get()
            ->toArray();

        foreach ($this->categories as $i => &$category) {
            foreach ($category['products'] as $j => &$product) {
                if(!isset($product['store'])) {
                    array_splice($category['products'], $j, 1);
                }
            }

            if(empty($category['products'])) {
                array_splice($this->categories, $i, 1);
            }
        }

        $this->stores = Store::all()
            ->toArray();

        $this->products = Product::with('store', 'productCategory')
            ->whereHas('store', function ($q) {
                $q->whereNull('stores.deleted_at');
            })
            ->get()
            ->toArray();
    }

    public function render()
    {
        return view('landing/index')
            ->extends('layouts.landing-layout');
    }
}
