<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Store;
use App\Models\User;
use App\Services\MailService;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class CartForm extends Component
{
    public $phone = '';
    public $address = '';
    public $city = '';
    public $notes = '';

    protected $rules = [
        'phone' => 'required',
        'address' => 'required',
        'city' => 'required',
    ];

    protected $messages = [
        'phone.required' => 'Ju lutem vendosni numrin tuaj te kontaktit.',
        'address.required' => 'Ju lutem vendosni adresen tuaj.',
        'city.required' => 'Ju lutem vendosni qytetin tuaj.'
    ];

    public function order(MailService $mailService)
    {
        $this->validate();

        $cartProducts = \Cart::getContent();
        $total = \Cart::getTotal();

        if (count($cartProducts) <= 1 || $total == 0) {
            return $this->addError('products', 'Porosia nuk mund te dergohet pasi shporta nuk ka produkte.');
        }

        $user = User::find(auth()->user()->id);
        $user->phone_number = $this->phone;
        $user->address = $this->address;
        $user->city = $this->city;
        $user->save();

        $order = new Order;
        $order->consumer_id = auth()->user()->id;
        $order->total_price = $total;
        $order->delivery_price = $cartProducts['delivery']['price'];
        $order->note = $this->notes;
        $order->address = $this->address;
        $order->city = $this->city;
        $order->status = Order::$PENDING_STATUS;

        $orderProducts = array();
        foreach ($cartProducts as $cartProduct) {
            if ($cartProduct['id'] !== 'delivery') {
                $order->store_id = $cartProduct['attributes']['storeId'];

                $orderProduct = new OrderProduct;
                $orderProduct->name = $cartProduct['name'];
                $orderProduct->image_url = $cartProduct['attributes']['imageUrl'];
                $orderProduct->product_id = $cartProduct['id'];
                $orderProduct->quantity = $cartProduct['quantity'];
                $orderProduct->unit_price = $cartProduct['price'];
                $orderProduct->total_price = $cartProduct['price'] * $cartProduct['quantity'];

                $orderProducts[] = $orderProduct;
            }
        }

        $order->save();
        $order->products()->saveMany($orderProducts);

        \Cart::clear();

        $store = Store::with('owner')->where('id', $order->store_id)->first();
        if ($store !== null) {
            $mailService->sendNewOrderEmail($store->owner->email, $order->id);
        }

        $admins = User::where('role', 'admin')->get();
        foreach ($admins as $admin) {
            $mailService->sendNewOrderEmail($admin->email, $order->id);
        }

        redirect()->route('order-completion', ['id' => $order->id, 'ardhurNgaShporta' => true]);
    }

    public function mount(): void
    {
        $this->phone = auth()->user()->phone_number;
        $this->address = auth()->user()->address;
        $this->city = auth()->user()->city;
    }

    public function render()
    {
        return view('livewire.cart-form');
    }
}
