<?php

namespace App\Http\Livewire;

use App\Models\ProductCategory;
use Livewire\Component;

class ProductCategoryListRow extends Component
{
    public $category;

    public function delete():void
    {
        $p = ProductCategory::find($this->category->id);
        $p->delete();
        $this->category = null;
    }

    public function render()
    {
        return view('livewire.product-category-list-row');
    }
}
