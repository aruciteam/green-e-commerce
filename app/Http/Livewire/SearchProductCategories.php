<?php

namespace App\Http\Livewire;

use App\Models\ProductCategory;
use Livewire\Component;

class SearchProductCategories extends Component
{

    public $query;
    public $categories;

    public function mount(): void
    {
        $this->resetValues();
    }

    public function resetValues(): void
    {
        $this->query = '';
        $this->categories = [];
    }

    public function selectCategory($id): void
    {
            $this->redirect(route('category', $id));
    }

    public function search(): void
    {
        $this->categories = ProductCategory::where('name', 'like', '%' . $this->query . '%')
            ->with('products', 'products.store')
            ->whereHas('products.store', function ($q) {
                $q->whereNull('stores.deleted_at');
            })
            ->limit(5)
            ->get()
            ->toArray();
    }


    public function render()
    {
        $this->search();
        return view('livewire.search-product-categories');
    }
}
