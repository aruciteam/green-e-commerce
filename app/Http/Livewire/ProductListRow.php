<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class ProductListRow extends Component
{
    public $product;
    public $fromCategory;

    public function delete():void
    {
        $p = Product::find($this->product->id);
        $p->delete();
        $this->product = null;
    }

    public function render()
    {
        return view('livewire.product-list-row');
    }
}
