<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class EditProfile extends Component
{
    public $name;
    public $email;
    public $phone;
    public $address;
    public $city;

    protected $rules = [
        'name' => 'required|string|between:2,100',
        'phone' => 'required|between:6,100',
        'address' => 'required|string',
        'city' => 'required|string',
    ];

    protected $messages = [
        'name.required' => 'Ju lutem vendosni emrin tuaj.',
        'name.between' => 'Emri duhet te permbaje te pakten 2 karaktere.',
        'phone.required' => 'Ju lutem vendosni numrin tuaj te kontaktit.',
        'phone.between' => 'Numri juaj duhet te permbaje te pakten 6 karaktere.',
        'address.required' => 'Ju lutem vendosni adresen tuaj.',
        'city.required' => 'Ju lutem vendosni qytetin tuaj.',
    ];

    public function mount():void
    {
        $this->name = auth()->user()->name;
        $this->email = auth()->user()->email;
        $this->phone = auth()->user()->phone_number;
        $this->address = auth()->user()->address;
        $this->city = auth()->user()->city;
    }

    public function update(): void
    {
        $this->validate();

        $user = User::find(auth()->user()->id);

        $user->name =  $this->name;
        $user->phone_number =  $this->phone;
        $user->address =  $this->address;
        $user->city =  $this->city;

        $user->save();
    }

    public function render()
    {
        return view('livewire.edit-profile');
    }
}
