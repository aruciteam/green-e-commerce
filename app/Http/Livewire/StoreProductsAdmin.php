<?php


namespace App\Http\Livewire;


use App\Models\Product;
use App\Models\Store;
use Livewire\Component;

class StoreProductsAdmin extends Component
{
    public $storeId;
    public $products;

    public function mount($id):void
    {
        if(auth()->user()->isAdmin()) {
            $this->storeId = $id;

            $store = Store::where('id', $id)->get()[0];

            $this->products = Product::with('productCategory')
                ->where('store_id', $store->id)
                ->orderByDesc('id')
                ->get();
        } else {
            if(auth()->user()->isConsumer()) {
                redirect()->route('index');
            } else {
                redirect()->route('orders');
            }
        }
    }

    public function render()
    {
        return view('livewire.store-products')
            ->extends('layouts.app');
    }
}
