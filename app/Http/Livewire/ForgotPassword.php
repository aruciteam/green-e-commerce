<?php

namespace App\Http\Livewire;

use App\Services\MailService;
use Carbon\Carbon;
use DB;
use Livewire\Component;
use App\Models\User;

use Illuminate\Notifications\Notifiable;

class ForgotPassword extends Component
{
    use Notifiable;

    public $email = '';

    public $showSuccessNotification = false;
    public $showFailureNotification = false;

    protected $rules = [
        'email' => 'required|email',
    ];

    protected $messages = [
        'email.required' => 'Ju lutem vendosni email-in tuaj.',
        'email.email' => 'Ju lutem vendosni nje email te sakte.',
    ];

    public function mount(): void
    {
        if (auth()->user()) {
            redirect()->route('index');
        }
    }

    /**
     * @throws \Exception
     */
    public function recoverPassword(MailService $mailService): void
    {
        $this->validate();
        $user = User::where('email', $this->email)->first();
        if ($user) {
            $token = bin2hex(random_bytes(16));
            DB::table('password_resets')->insert([
                'email' => $this->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $mailService->sendForgotPasswordEmail($this->email, $token);

            $this->showSuccessNotification = true;
            $this->showFailureNotification = false;
        } else {
            $this->showFailureNotification = true;
        }
    }

    public function render()
    {
        return view('livewire.forgot-password')
            ->extends('layouts.landing-layout');
    }
}
