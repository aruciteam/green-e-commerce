<?php


namespace App\Http\Livewire;


use App\Models\Store;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreateStore extends Component
{
    use WithFileUploads;

    public $name;
    public $nipt;
    public $description;
    public $delivery_price;
    public $address;
    public $phone_number;
    public $image_url;

    public $photo;
    public $isPhotoUploaded = false;

    public $owner_name;
    public $email;
    public $owner_password;
    public $owner_phone;
    public $owner_address;
    public $owner_city;

    public $success = false;

    protected $rules = [
        'photo' => 'nullable|image',

        'name' => 'required|max:255',
        'description' => 'required|max:255',
        'delivery_price' => 'required|integer',
        'phone_number' => 'required',
        'address' => 'required',

        'owner_name' => 'required|string|between:2,100',
        'email' =>  'required|string|max:100|unique:users|email:rfc,dns',
        'owner_password' => 'required|string|min:6',
        'owner_address' => 'required',
        'owner_city' => 'required',
        'owner_phone' => 'required|between:6,100',
    ];

    protected $messages = [
        'photo.image' => 'Formati duhet te jete imazh.',
        'name.required' => 'Ju lutem vendosni emrin e dyqanit.',
        'description.required' => 'Ju lutem vendosni pershkrimin e dyqanit.',
        'phone_number.required' => 'Ju lutem vendosni njesine numrin e kontaktit te dyqanit.',
        'delivery_price.required' => 'Ju lutem vendosni koston e transportit te dyqanit.',
        'delivery_price.integer' => 'Kosto e transportit e dyqanit duhet te jete numer.',
        'address.required' => 'Ju lutem vendosni adresen e dyqanit.',

        'email.required' => 'Ju lutem vendosni email-in e menaxherit.',
        'v.email' => 'Ju lutem vendosni nje email te sakte.',
        'email.unique' => 'Nje profil me kete email tashme ekziston.',
        'owner_password.required' => 'Ju lutem vendosni fjalekalimin e menaxherit.',
        'owner_password.min' => 'Ju lutem vendosni nje fjalekalim me te pakten 6 karaktere.',
        'owner_name.required' => 'Ju lutem vendosni emrin e menaxherit.',
        'owner_name.between' => 'Emri duhet te permbaje te pakten 2 karaktere.',
        'owner_address.required' => 'Ju lutem vendosni emrin e menaxherit.',
        'owner_city.required' => 'Ju lutem vendosni emrin e menaxherit.',
        'owner_phone.required' => 'Ju lutem vendosni numrin e kontaktit te menaxherit.',
        'owner_phone.between' => 'Numri duhet te permbaje te pakten 6 karaktere.',
    ];

    public function updatedPhoto(): void
    {
        $this->isPhotoUploaded = true;
    }

    public function save(): void
    {
        $this->success = false;
        $this->validate();

        $photoUrl = null;
        if ($this->isPhotoUploaded) {
            $photoName = $this->photo->store('public');
            $photoUrl = env('APP_URL') . Storage::url($photoName);
        }

        $s = new Store;

        $s->name = $this->name;
        $s->nipt = $this->nipt;
        $s->description = $this->description;
        $s->delivery_price = $this->delivery_price;
        $s->address = $this->address;
        if ($this->isPhotoUploaded) {
            $s->image_url = $photoUrl;
        }
        $s->phone_number = $this->phone_number;

        $user = User::create([
            'name' => $this->owner_name,
            'email' => $this->email,
            'password' => Hash::make($this->owner_password),
            'phone_number' => $this->owner_phone,
            'address' => $this->owner_address,
            'city' => $this->owner_city,
            'role' => User::$VENDOR,
        ]);

        $s->owner_id = $user->id;

        $s->save();

        $this->success = true;
        redirect()->route('stores-list');
    }

    public function mount(): void
    {
        if (auth()->user()->isConsumer()) {
            redirect()->route('index');
        } else if(auth()->user()->isVendor()) {
            redirect()->route('orders');
        }
    }

    public function render()
    {
        return view('livewire.create-store')
            ->extends('layouts.app');

    }

}
