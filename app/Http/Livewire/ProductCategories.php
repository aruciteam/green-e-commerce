<?php


namespace App\Http\Livewire;


use App\Models\ProductCategory;
use Livewire\Component;

class ProductCategories extends Component
{
    public $category;

    public function mount($id): void
    {
        $this->category = ProductCategory::with('products', 'products.store', 'products.productCategory')
            ->where('id', $id)
            ->get()
            ->toArray()[0];

        foreach ($this->category['products'] as $i => &$product) {
            $product['category_color'] = $this->category['color'];
            if(!isset($product['store'])) {
                array_splice($this->category['products'], $i, 1);
            }
        }
    }

    public function render()
    {
        return view('landing/product-category')
            ->extends('layouts.landing-layout');
    }
}
