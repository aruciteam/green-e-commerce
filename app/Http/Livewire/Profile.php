<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Profile extends Component
{
    public $orders;

    public function mount(): void
    {
        if (!auth()->user()) {
            redirect()->route('index');
        }

        $this->orders = Order::with('consumer', 'store', 'store.owner', 'products')
            ->where('consumer_id', Auth::id())
            ->orderByDesc('id')
            ->get()
            ->toArray();
    }

    public function render()
    {
        if(auth()->user()->isConsumer()) {
            return view('landing/profile')
                ->extends('layouts.landing-layout');
        }

        return view('landing/profile')
            ->extends('layouts.app');
    }
}
