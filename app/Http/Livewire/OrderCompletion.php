<?php


namespace App\Http\Livewire;


use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class OrderCompletion extends Component
{
    public $order;
    public $isFromCart;

    public function mount($id): void
    {
        if (!auth()->user()) {
            redirect()->route('index');
        }

        $this->isFromCart = request()->query('ardhurNgaShporta');


        $orders = Order::with('consumer', 'store', 'store.owner', 'products')
            ->where(['id' => $id, 'consumer_id' => Auth::id()])
            ->get()
            ->toArray();

        if(!empty($orders)) {
            $this->order= $orders[0];
        } else {
            redirect()->route('index');
        }
    }

    public function render()
    {
        return view('landing/order-completion')
            ->extends('layouts.landing-layout');
    }
}
