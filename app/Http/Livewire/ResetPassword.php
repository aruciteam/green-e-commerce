<?php

namespace App\Http\Livewire;

use App\Models\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ResetPassword extends Component
{
    public $email;
    public $code;

    public $password = '';

    public $showSuccessNotification = false;
    public $showFailureNotification = false;

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];

    protected $messages = [
        'email.required' => 'Ju lutem vendosni email-in tuaj.',
        'email.email' => 'Ju lutem vendosni nje email te sakte.',
        'password.required' => 'Ju lutem vendosni fjalekalimin tuaj.',
        'password.min' => 'Fjalekalimi juaj duhet te kete te pakten 6 karaktere.'
    ];


    public function mount(): void
    {
        $this->email = request()->get('email');
        $this->code =  request()->get('code');

        $exists = DB::table('password_resets')
            ->where(['token' => $this->code, 'email' => $this->email])->exists();

        if(!$exists) {
            redirect()->route('index');
        }
    }

    public function resetPassword(): void
    {
        $this->validate();

        $exists = DB::table('password_resets')
            ->where(['token' => $this->code, 'email' => $this->email])->exists();

        if($exists) {
            $existingUser = User::where('email', $this->email)->first();

            if ($existingUser) {
                $existingUser->update([
                    'password' => Hash::make($this->password)
                ]);
                $this->showSuccessNotification = true;
                $this->showFailureNotification = false;

                DB::table('password_resets')
                    ->where(['token' => $this->code, 'email' => $this->email])
                    ->delete();

                redirect()->route('login');
            } else {
                $this->showFailureNotification = true;
            }
        }

    }

    public function render()
    {
        return view('livewire.reset-password')
            ->extends('layouts.landing-layout');
    }
}

