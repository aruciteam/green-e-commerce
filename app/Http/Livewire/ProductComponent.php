<?php


namespace App\Http\Livewire;


use Livewire\Component;

class ProductComponent extends Component
{
    public $product;
    public $quantity;
    public $canBeAddedToCart;
    public $showProductAddedNotification;

    public function addQuantity()
    {
        $this->quantity++;
    }

    public function subtractQuantity()
    {
        if ($this->quantity > 1) {
            $this->quantity--;
        }
    }

    public function addToCart() {
        $storeId = $this->product['store']['id'];

        $items = \Cart::getContent();
        foreach($items as $row) {
            if($row->attributes->storeId !== $storeId) {
                \Cart::clear();
                break;
            }
        }

        \Cart::add(array(
            'id' => $this->product['id'],
            'name' => $this->product['name'],
            'price' => $this->product['unit_price'],
            'quantity' => $this->quantity,
            'attributes' => array(
                'imageUrl' => $this->product['image_url'],
                'storeId' => $storeId
            ),
            'associatedModel' => $this->product,
        ));

        \Cart::remove('delivery');
        \Cart::add(array(
                'id' => 'delivery',
                'name' => 'Transporti',
                'price' => $this->product['store']['delivery_price'],
                'quantity' => 1,
                'attributes' => array(
                    'storeId' => $storeId
                ),
            )
        );

        $this->quantity = 1;
        $this->showProductAddedNotification = true;
    }

    public function mount(): void
    {
        $this->quantity = 1;
    }

    public function render()
    {
        return view('livewire.product');
    }
}
