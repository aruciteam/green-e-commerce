<?php

namespace App\Http\Livewire;

use App\Models\Store;
use Livewire\Component;

class StoreListRow extends Component
{
    public $store;

    public function delete():void
    {
        $p = Store::find($this->store->id);
        $p->delete();
        $this->store = null;
    }

    public function render()
    {
        return view('livewire.store-list-row');
    }
}
