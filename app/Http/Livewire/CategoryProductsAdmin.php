<?php


namespace App\Http\Livewire;


use App\Models\Product;
use App\Models\ProductCategory;
use Livewire\Component;

class CategoryProductsAdmin extends Component
{
    public $categoryId;
    public $products;
    public $fromCategory = true;

    public function mount($id):void
    {
        if(auth()->user()->isAdmin()) {
            $this->categoryId = $id;

            $category = ProductCategory::where('id', $id)->get()[0];

            $this->products = Product::with('productCategory', 'store')
                ->where('product_category_id', $category->id)
                ->whereHas('store', function ($q) {
                    $q->whereNull('stores.deleted_at');
                })
                ->orderByDesc('id')
                ->get();
        } else {
            if(auth()->user()->isConsumer()) {
                redirect()->route('index');
            } else {
                redirect()->route('orders');
            }
        }
    }

    public function render()
    {
        return view('livewire.category-products-admin')
            ->extends('layouts.app');
    }
}
