<?php


namespace App\Http\Livewire;


use App\Models\Order;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class StoreOrders extends Component
{
    public $orders;

    public function mount():void
    {
        if(!auth()->user()->isConsumer()) {
            if(auth()->user()->isAdmin()) {
                $this->orders = Order::with('consumer', 'store', 'store.owner', 'products')
                    ->orderByDesc('id')
                    ->get();
            } else if(auth()->user()->isVendor()) {
                $store = Store::where('owner_id', Auth::id())->get()[0];
                \Log::info($store);

                $this->orders = Order::with('consumer', 'store', 'store.owner', 'products')
                    ->where('store_id', $store->id)
                    ->orderByDesc('id')
                    ->get();
            }
        } else {
            redirect()->route('index');
        }
    }

    public function render()
    {
        return view('store/orders')
            ->extends('layouts.app');
    }
}
