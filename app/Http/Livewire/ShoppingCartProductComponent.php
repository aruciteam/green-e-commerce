<?php


namespace App\Http\Livewire;


use Livewire\Component;

class ShoppingCartProductComponent extends Component
{
    public $product;
    public $quantity;
    public $show = true;

    public function addQuantity(): void
    {
        $this->quantity++;

        \Cart::update($this->product['id'],[
            'quantity' => 1,
        ]);
    }

    public function subtractQuantity(): void
    {
        if ($this->quantity > 1) {
            $this->quantity--;

            \Cart::update($this->product['id'],[
                'quantity' => -1,
            ]);
        }
    }

    public function removeProduct(): void
    {
        \Cart::remove($this->product['id']);
        $this->show = false;
    }

    public function mount(): void
    {
        $this->quantity = $this->product['quantity'];
    }

    public function render()
    {
        return view('livewire.shopping-cart-product');
    }
}
