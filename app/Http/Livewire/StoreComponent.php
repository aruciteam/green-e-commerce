<?php


namespace App\Http\Livewire;


use App\Models\Store;
use Livewire\Component;

class StoreComponent extends Component
{
    public $store;

    public function mount($id): void
    {
        $this->store = Store::with('products', 'products.productCategory', 'products.store')->where('id', $id)
            ->get()
            ->toArray()[0];
    }

    public function render()
    {
        return view('landing/store')
            ->extends('layouts.landing-layout');
    }
}
