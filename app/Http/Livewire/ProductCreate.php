<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Store;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProductCreate extends Component
{
    use WithFileUploads;

    public $categories;

    public $name;
    public $description;
    public $unit_name;
    public $unit_price;
    public $product_category_id;

    public $photo;
    public $isPhotoUploaded = false;

    protected $rules = [
        'photo' => 'nullable|image',

        'name' => 'required|max:255',
        'description' => 'required|max:255',
        'unit_name' => 'required|max:255',
        'unit_price' => 'required|integer',
        'product_category_id' => 'required',
    ];

    protected $messages = [
        'photo.image' => 'Formati duhet te jete imazh.',
        'name.required' => 'Ju lutem vendosni emrin e produktit.',
        'description.required' => 'Ju lutem vendosni pershkrimin e produktit.',
        'unit_name.required' => 'Ju lutem vendosni njesine baze te produktit si psh: Kg.',
        'unit_price.required' => 'Ju lutem vendosni cmimin per nje njesi te produktit.',
        'unit_price.integer' => 'Cmimi i njesise se produktit duhet te jete numer.',
        'product_category_id.required' => 'Ju lutem zgjidhni kategorine te se ciles produkti i perket.',
    ];

    public function updatedPhoto(): void
    {
        $this->isPhotoUploaded = true;
    }

    public function save(): void
    {
        $this->validate();

        $photoUrl = null;
        if ($this->isPhotoUploaded) {
            $photoName = $this->photo->store('public');
            $photoUrl = Storage::url($photoName);
        }

        $p = new Product;

        $p->name = $this->name;
        $p->description = $this->description;
        $p->unit_name = $this->unit_name;
        $p->unit_price = $this->unit_price;
        if ($this->isPhotoUploaded) {
            $p->image_url = $photoUrl;
        }
        $p->product_category_id = $this->product_category_id;

        $store = Store::where('owner_id', auth()->user()->id)->first();
        $p->store_id = $store->id;

        $p->save();

        redirect()->route('store-products');
    }

    public function setCategory($categoryId): void
    {
        $this->product_category_id = $categoryId;
    }

    public function mount(): void
    {
        if (auth()->user()->isConsumer()) {
            redirect()->route('index');
        } else if (auth()->user()->isAdmin()) {
            redirect()->route('orders');
        } else {
            $this->categories = ProductCategory::all();
        }
    }

    public
    function render()
    {
        return view('livewire.product-save')
            ->extends('layouts.app');
    }
}
