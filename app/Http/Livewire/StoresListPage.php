<?php

namespace App\Http\Livewire;

use App\Models\Store;
use Livewire\Component;

class StoresListPage extends Component
{
    public $stores;

    public function mount():void
    {
        $this->stores = Store::all();
    }

    public function render()
    {
        return view('livewire.stores-list-page')
            ->extends('layouts.app');
    }
}
