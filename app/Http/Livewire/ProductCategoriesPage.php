<?php

namespace App\Http\Livewire;

use App\Models\ProductCategory;
use Livewire\Component;
use function Symfony\Component\String\s;

class ProductCategoriesPage extends Component
{
    public $categories;

    public function mount(): void
    {
        $this->categories = ProductCategory::all();
    }

    public function render()
    {
        return view('livewire.product-categories-page')
            ->extends('layouts.app');
    }
}
