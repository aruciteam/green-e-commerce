<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ChangePassword extends Component
{
    public $password;
    public $newPassword;

    public $success = false;

    protected $rules = [
        'password' => 'required|min:6',
        'newPassword' => 'required|min:6'
    ];

    protected $messages = [
        'password.required' => 'Ju lutem vendosni fjalekalimin tuaj aktual.',
        'newPassword.required' => 'Ju lutem vendosni fjalekalimin tuaj te ri.',
        'password.min' => 'Fjalekalimi juaj duhet te kete te pakten 6 karaktere.',
        'newPassword.min' => 'Fjalekalimi juaj duhet te kete te pakten 6 karaktere.'
    ];

    public function changePassword()
    {
        $this->validate();

        $user = User::find(auth()->user()->id);

        if(Hash::check($this->password, $user->password)) {
            $user->update([
                'password' => Hash::make($this->newPassword)
            ]);

            $this->success = true;
        } else {
            return $this->addError('password', 'Fjalekalimi aktual nuk eshte i sakte.');
        }
    }

    public function render()
    {
        return view('livewire.change-password');
    }
}
