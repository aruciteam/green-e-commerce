<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class OrderInfoPage extends Component
{
    public $order;

    public function mount($id): void
    {
        $this->order = Order::with('consumer', 'store', 'store.owner', 'products')
            ->where('id', $id)
            ->first();


        if (auth()->user()->isVendor()) {
            if($this->order->store->owner->id !== \Auth::id()) {
                redirect()->route('orders');
            }
        } else if(auth()->user()->isConsumer()) {
            redirect()->route('index');
        }

    }

    public function accept(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$ACCEPTED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$ACCEPTED_STATUS;
    }

    public function reject(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$REJECTED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$REJECTED_STATUS;
    }

    public function complete(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$COMPLETED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$COMPLETED_STATUS;
    }

    public function render()
    {
        return view('livewire.order-info-page')
            ->extends('layouts.app');
    }
}
