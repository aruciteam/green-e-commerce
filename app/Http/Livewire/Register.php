<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Notifications\VerifyEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Register extends Component
{
    public $email;
    public $name;
    public $password;
    public $acceptedPrivacyPolicy;
    public $isFromCart = false;

    protected $rules = [
        'name' => 'required|string|between:2,100',
        'email' => 'required|string|max:100|unique:users|email:rfc,dns',
        'password' => 'required|string|min:6',
        'acceptedPrivacyPolicy' => 'required',
    ];

    protected $messages = [
        'email.required' => 'Ju lutem vendosni email-in tuaj.',
        'email.email' => 'Ju lutem vendosni nje email te sakte.',
        'email.unique' => 'Nje profil me kete email tashme ekziston.',
        'password.required' => 'Ju lutem vendosni fjalekalimin tuaj.',
        'password.min' => 'Ju lutem vendosni nje fjalekalim me te pakten 6 karaktere.',
        'name.required' => 'Ju lutem vendosni emrin tuaj.',
        'name.between' => 'Emri duhet te permbaje te pakten 2 karaktere.',
        'acceptedPrivacyPolicy.required' => 'Ju lutem pranoni Politikat e Privatesise.'
    ];

    public function mount(): void
    {
        $this->isFromCart = request()->query('ardhurNgaShporta');

        if (auth()->user()) {
            redirect()->route('index');
        }
    }

    public function register(): void
    {
        $this->validate();

        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'role' => User::$CONSUMER,
        ]);

        auth()->login($user);
        $user->notify(new VerifyEmail());

        if ($user->isAdmin()) {
            redirect()->route('index');
        } else if ($user->isVendor()) {
            redirect()->route('index');
        } else if (isset($this->isFromCart) && $this->isFromCart == 1) {
            redirect()->route('cart');
        } else {
            redirect()->route('index');
        }
    }

    public function render()
    {
        return view('livewire.register');
    }
}
