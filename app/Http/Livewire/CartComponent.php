<?php


namespace App\Http\Livewire;

use Livewire\Component;

class CartComponent extends Component
{
    public $deliveryPrice;
    public $products = array();
    public $total;

    public function mount(): void
    {
        if (!auth()->user()) {
            redirect()->route('login', ['ardhurNgaShporta' => true]);
        }

        $this->calcCart();
    }

    public function render()
    {
        return view('landing/cart')
            ->extends('layouts.landing-layout');
    }

    public function calcCart(): void
    {
        $cartProducts = \Cart::getContent();

        foreach ($cartProducts as $cartProduct) {
            $product = array(
                'id' => $cartProduct->id,
                'name' => $cartProduct->name,
                'price' => $cartProduct->price,
                'quantity' => $cartProduct->quantity,
                'imageUrl' => $cartProduct->attributes->imageUrl,
                'storeId' => $cartProduct->attributes->storeId,
            );

            $this->products[$cartProduct->id] = $product;
        }

        $this->total = \Cart::getTotal();
    }
}
