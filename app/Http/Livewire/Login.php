<?php


namespace App\Http\Livewire;


use App\Models\Store;
use App\Models\User;
use Livewire\Component;

class Login extends Component
{

    public $email = '';
    public $password = '';
    public $remember_me = false;
    public $isFromCart = false;

    protected $rules = [
        'email' => 'required|email:rfc,dns',
        'password' => 'required',
    ];

    protected $messages = [
        'email.required' => 'Ju lutem vendosni email-in tuaj.',
        'email.email' => 'Ju lutem vendosni nje email te sakte.',
        'password.required' => 'Ju lutem vendosni fjalekalimin tuaj.'
    ];

    public function mount(): void
    {
        $this->isFromCart = request()->query('ardhurNgaShporta');

        if (auth()->user()) {
            if (auth()->user()->isAdmin()) {
                redirect()->route('products');
            } else if (auth()->user()->isVendor()) {
                redirect()->route('index');
            }
            redirect()->route('index');
        }
    }

    public function login(): \Illuminate\Support\MessageBag
    {
        $this->validate();

        if (auth()->attempt(['email' => $this->email, 'password' => $this->password], $this->remember_me)) {
            $user = User::where(["email" => $this->email])->first();
            if (isset($user)) {
                if($user->isVendor()) {
                    $s = Store::where('owner_id', $user->id)->first();
                    if(!isset($s)) {
                        return $this->addError('password', 'Dyqani juaj eshte fshire. Ju lutem kontaktoni administratorin nese mendoni qe ky eshte nje gabim.');
                    }
                }

                auth()->login($user, $this->remember_me);
                if ($user->isAdmin()) {
                    redirect()->route('orders');
                } else if ($user->isVendor()) {
                    redirect()->route('orders');
                } else if (isset($this->isFromCart) && $this->isFromCart == 1) {
                    redirect()->route('cart');
                } else {
                    redirect()->route('index');
                }
            } else {
                return $this->addError('password', 'Kredencialet nuk ishin te sakta');
            }
        }

        return $this->addError('password', trans('Kredencialet nuk ishin te sakta'));
    }

    public function render()
    {
        return view('livewire.login');

    }
}
