<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class StoreProducts extends Component
{
    public $products;

    public function mount():void
    {
        if(auth()->user()->isVendor()) {
            $store = Store::where('owner_id', Auth::id())->get()[0];

            $this->products = Product::with('productCategory')
                ->where('store_id', $store->id)
                ->orderByDesc('id')
                ->get();
        } else {
            if(auth()->user()->isConsumer()) {
                redirect()->route('index');
            } else {
                redirect()->route('orders');
            }
        }
    }

    public function render()
    {
        return view('livewire.store-products')
            ->extends('layouts.app');
    }
}
