<?php


namespace App\Http\Livewire;


use App\Models\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreateProductCategory extends Component
{
    use WithFileUploads;

    public $category;
    public $isCreateCategory = true;

    public $name;
    public $description;
    public $type;
    public $color;
    public $image_url;

    public $photo;
    public $isPhotoUploaded = false;

    public $success = false;

    protected $rules = [
        'photo' => 'nullable|image',

        'name' => 'required|max:255',
        'description' => 'required|max:255',
        'type' => 'required',
        'color' => 'required',
    ];

    protected $messages = [
        'photo.image' => 'Formati duhet te jete imazh.',
        'name.required' => 'Ju lutem vendosni emrin e kategorise.',
        'description.required' => 'Ju lutem vendosni pershkrimin e kategorise.',
        'type.required' => 'Ju lutem vendosni llojin e kategorise.',
        'color.required' => 'Ju lutem vendosni kodin hexadecimal te ngjyres se kategorise (psh: #eed352).',
    ];

    public function updatedPhoto(): void
    {
        $this->isPhotoUploaded = true;
    }

    public function save(): void
    {
        $this->success = false;
        $this->validate();

        $photoUrl = null;
        if ($this->isPhotoUploaded) {
            $photoName = $this->photo->store('public');
            $photoUrl = env('APP_URL') . Storage::url($photoName);
        }

        $p = new ProductCategory;

        $p->name = $this->name;
        $p->description = $this->description;
        $p->type = $this->type;
        $p->color = $this->color;
        if ($this->isPhotoUploaded) {
            $p->image_url = $photoUrl;
        }

        $p->save();

        $this->success = true;
        redirect()->route('categories-list');
    }

    public function mount(): void
    {
        if (auth()->user()->isConsumer()) {
            redirect()->route('index');
        } else if(auth()->user()->isVendor()) {
            redirect()->route('orders');
        }
    }

    public function render()
    {
        return view('livewire.product-category-edit')
            ->extends('layouts.app');
    }
}
