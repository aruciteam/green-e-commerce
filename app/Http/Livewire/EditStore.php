<?php

namespace App\Http\Livewire;

use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditStore extends Component
{
    use WithFileUploads;


    public $store;

    public $name;
    public $description;
    public $delivery_price;
    public $address;
    public $phone_number;
    public $image_url;

    public $photo;
    public $isPhotoUploaded = false;

    public $success = false;

    protected $rules = [
        'photo' => 'nullable|image',

        'name' => 'required|max:255',
        'description' => 'required|max:255',
        'delivery_price' => 'required|integer',
        'phone_number' => 'required',
        'address' => 'required',
    ];

    protected $messages = [
        'photo.image' => 'Formati duhet te jete imazh.',
        'name.required' => 'Ju lutem vendosni emrin e dyqanit.',
        'description.required' => 'Ju lutem vendosni pershkrimin e dyqanit.',
        'phone_number.required' => 'Ju lutem vendosni njesine numrin e kontaktit te dyqanit.',
        'delivery_price.required' => 'Ju lutem vendosni koston e transportit te dyqanit.',
        'delivery_price.integer' => 'Kosto e transportit e dyqanit duhet te jete numer.',
        'address.required' => 'Ju lutem vendosni adresen e dyqanit.',
    ];

    public function updatedPhoto(): void
    {
        $this->isPhotoUploaded = true;
    }

    public function save():void {
        $this->success = false;
        $this->validate();

        $photoUrl = null;
        if ($this->isPhotoUploaded) {
            $photoName = $this->photo->store('public');
            $photoUrl = env('APP_URL') . Storage::url($photoName);
        }

        $s = Store::find($this->store->id);

        $s->name = $this->name;
        $s->description = $this->description;
        $s->delivery_price = $this->delivery_price;
        $s->address = $this->address;
        if ($this->isPhotoUploaded) {
            $s->image_url = $photoUrl;
        }
        $s->phone_number = $this->phone_number;

        $s->save();

        $this->success = true;
    }

    public function mount():void
    {
        if(auth()->user()->isVendor()) {
            $this->store = Store::where('owner_id', Auth::id())->first();

            $this->name = $this->store->name;
            $this->description = $this->store->description;
            $this->delivery_price = $this->store->delivery_price;
            $this->address = $this->store->address;
            $this->phone_number = $this->store->phone_number;
            $this->image_url = $this->store->image_url;
        } else {
            if(auth()->user()->isConsumer()) {
                redirect()->route('index');
            } else {
                redirect()->route('orders');
            }
        }
    }

    public function render()
    {
        return view('livewire.edit-store')
            ->extends('layouts.app');
    }
}
