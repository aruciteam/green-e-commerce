<?php

namespace App\Http\Livewire;

use App\Models\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProductCategoryEdit extends Component
{
    use WithFileUploads;

    public $category;

    public $name;
    public $description;
    public $type;
    public $color;
    public $image_url;

    public $photo;
    public $isPhotoUploaded = false;

    public $success = false;

    protected $rules = [
        'photo' => 'nullable|image',

        'name' => 'required|max:255',
        'description' => 'required|max:255',
        'type' => 'required',
        'color' => 'required',
    ];

    protected $messages = [
        'photo.image' => 'Formati duhet te jete imazh.',
        'name.required' => 'Ju lutem vendosni emrin e kategorise.',
        'description.required' => 'Ju lutem vendosni pershkrimin e kategorise.',
        'type.required' => 'Ju lutem vendosni llojin e kategorise.',
        'color.required' => 'Ju lutem vendosni kodin hexadecimal te ngjyres se kategorise (psh: #eed352).',
    ];

    public function updatedPhoto(): void
    {
        $this->isPhotoUploaded = true;
    }

    public function save(): void
    {
        $this->success = false;
        $this->validate();

        $photoUrl = null;
        if ($this->isPhotoUploaded) {
            $photoName = $this->photo->store('public');
            $photoUrl = env('APP_URL') . Storage::url($photoName);
        }

        $p = ProductCategory::find($this->category->id);

        $p->name = $this->name;
        $p->description = $this->description;
        $p->type = $this->type;
        $p->color = $this->color;
        if ($this->isPhotoUploaded) {
            $p->image_url = $photoUrl;
        }

        $p->save();

        $this->success = true;
        redirect()->route('categories-list');
    }

    public function mount($id): void
    {
        if (auth()->user()->isAdmin()) {
            $this->category = ProductCategory::where('id', $id)->first();

            $this->name = $this->category->name;
            $this->description = $this->category->description;
            $this->type = $this->category->type;
            $this->color = $this->category->color;
            $this->image_url = $this->category->image_url;
        } else {
            if (auth()->user()->isConsumer()) {
                redirect()->route('index');
            } else {
                redirect()->route('orders');
            }
        }
    }


    public function render()
    {
        return view('livewire.product-category-edit')
            ->extends('layouts.app');
    }
}
