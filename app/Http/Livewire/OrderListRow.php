<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Livewire\Component;

class OrderListRow extends Component
{
    public $order;

    public function accept(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$ACCEPTED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$ACCEPTED_STATUS;
    }

    public function reject(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$REJECTED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$REJECTED_STATUS;
    }
    public function complete(): void
    {
        $dbOrder = Order::find($this->order->id);
        $dbOrder->status = Order::$COMPLETED_STATUS;
        $dbOrder->save();

        $this->order->status = Order::$COMPLETED_STATUS;
    }

    public function render()
    {
        return view('livewire.order-list-row');
    }
}
