<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class EnsureRoleIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $role
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $role)
    {
        if ($role === User::$CONSUMER && auth()->user()->role !== User::$CONSUMER) {
            abort(403);
        }

        if ($role === User::$VENDOR && auth()->user()->role !== User::$VENDOR) {
            abort(403);
        }

        if ($role === User::$ADMIN && auth()->user()->role !== User::$ADMIN) {
            abort(403);
        }

        return $next($request);
    }
}
