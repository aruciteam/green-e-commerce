<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $code)
    {
        $this->email = $email;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = route('reset-password') . '?email=' . $this->email . '&code=' . $this->code;

        return $this->view('emails.forgot-password-email')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->subject('Ferma Jeshile - Fjalekalim i ri')
            ->with([
                'url' => $url,
            ]);
    }
}
