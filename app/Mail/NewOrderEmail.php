<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewOrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = route('orders-info-page', ['id' => $this->id]);

        return $this->view('emails.new-order-email')
            ->from(env('MAIL_FROM_ADDRESS'))
            ->subject('Ferma Jeshile - Porosi e re')
            ->with([
                'url' => $url,
            ]);
    }
}
