<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $consumer_id
 * @property int $store_id
 * @property int $total_price
 * @property int $delivery_price
 * @property string $address
 * @property string $city
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $consumer
 * @property-read \App\Models\Store $store
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereConsumerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @property string $note
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 */
class Order extends Model
{
    use HasFactory;

    public static $PENDING_STATUS = 'NE PRITJE';
    public static $ACCEPTED_STATUS = 'PRANUAR';
    public static $COMPLETED_STATUS = 'PERFUNDUAR';
    public static $REJECTED_STATUS = 'REFUZUAR';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'consumer_id',
        'store_id',
        'total_price',
        'delivery_price',
        'note',
        'address',
        'city',
        'status'
    ];

    public function consumer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Store::class);
    }

    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function isPending(): bool
    {
        return $this->status === self::$PENDING_STATUS;
    }

    public function isAccepted(): bool
    {
        return $this->status === self::$ACCEPTED_STATUS;
    }

    public function isCompleted(): bool
    {
        return $this->status === self::$COMPLETED_STATUS;
    }

    public function isRejected(): bool
    {
        return $this->status === self::$REJECTED_STATUS;
    }
}
