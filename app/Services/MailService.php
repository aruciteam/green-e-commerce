<?php


namespace App\Services;


use App\Mail\ForgotPasswordEmail;
use App\Mail\NewOrderEmail;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public function sendForgotPasswordEmail($email, $code): void
    {
        Mail::to($email)->send(new ForgotPasswordEmail($email, $code));
    }

    public function sendNewOrderEmail($email, $id): void
    {
        Mail::to($email)->send(new NewOrderEmail($id));
    }
}
